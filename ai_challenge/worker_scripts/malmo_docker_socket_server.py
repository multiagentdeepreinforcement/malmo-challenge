# Village People, 2017

import torch

from env.village_env_agents import Binary18BatchAgentWrapper
from pig_chase.common import ENV_ACTIONS
from utils import AverageMeter

from agents import get_agent
import time
from termcolor import colored as clr
import os.path as path
import numpy as np
import random
from synchronised_episodic_tcp_players import\
    ServerForSynchronisedEpisodicPlayers
from utils.utils import SaveToCSV


def print_info(message):
    print(clr("[DOCKER_MASTER] ", "magenta") + message)


class MalmoDockerSocketServer(ServerForSynchronisedEpisodicPlayers):

    def __init__(self, clients_no, shared_objects, cfg):

        self.use_cuda = cfg.general.use_cuda
        self.prev_alive = torch.ByteTensor(clients_no).fill_(1)
        self._dummy_state = torch.LongTensor(1, 18, 9, 9)
        self._dummy_reward = torch.LongTensor([0])

        # -- Initialize agent and wrap it in a Binary18BatchAgentWrapper :)
        Agent = get_agent(cfg.agent.type)
        agent = Agent(cfg.agent.name, ENV_ACTIONS, cfg, shared_objects)
        agent_runner = Binary18BatchAgentWrapper(agent, cfg.agent.name, cfg)
        print_info(
            "{:s}<{:s}> agent is up and waiting to learn. |role={:d}".format(
                cfg.agent.name, cfg.agent.type, cfg.agent.role
            ))

        self.agent = agent
        self.agent_runner = agent_runner

        self.max_freq_r = -100
        self.max_freq_r_ep = -1

        self.viz_rewards = torch.LongTensor(clients_no).fill_(0)
        self.viz_steps = torch.LongTensor(clients_no).fill_(0)
        self.ep_rewards = []
        self.ep_steps = []

        self.episode_time = AverageMeter(
            save_path=path.join(agent.model_utils.save_path,
                                "episode_time"))
        self.reward_save = AverageMeter(
            save_path=path.join(agent.model_utils.save_path,
                                "reward_sum_step"))
        self.mean_r_step = AverageMeter(
            save_path=path.join(agent.model_utils.save_path,
                                "mean_r_step"))
        self.rewards_dist = SaveToCSV(
            path.join(agent.model_utils.save_path, "rewards_distribution"),
            ["Episode", "Frame", "Reward", "Count"]
        )
        self.report_freq = cfg.general.report_freq

        self.total_steps = 0
        self.start_episode_time = time.time()

        super(MalmoDockerSocketServer, self).__init__(clients_no,
                                                      cfg.docker.host,
                                                      cfg.docker.port,
                                                      1
                                                      )

    def check_state(self, state):
        return torch.is_tensor(state) and\
               state.size() == torch.Size([1, 18, 9, 9])

    def feedback(self, states, rewards):

        states = torch.cat(states, 0)
        rewards = torch.cat(rewards, 0)

        done = self.is_done
        prev_alive = self.prev_alive

        self.ep_steps.append((1 - self.is_done).long())
        self.ep_rewards.append(rewards.clone())

        if self.use_cuda:
            states = states.cuda()
            rewards = rewards.cuda()
            done = done.cuda()
            prev_alive = prev_alive.cuda()

        actions = self.agent_runner.act(states, rewards, done, prev_alive)

        self.prev_alive = 1 - self.is_done

        return actions

    def forget_current_episode(self):
        self.agent_runner.forget_current_batch()
        self.ep_rewards.clear()
        self.ep_steps.clear()


    def reset(self, ep_no):
        self.ep_no = ep_no
        self.prev_alive.fill_(1)
        self.agent_runner.reset()

    def episode_finished(self, states, rewards):
        ep_no = self.ep_no
        # -- Reporting and stuff below

        self.total_steps = self.total_steps + self.frame_no

        self.ep_rewards = self.ep_rewards[1:]

        for i, rew in enumerate(self.ep_rewards):
            self.reward_save.update(torch.sum(rew), index=(ep_no, i))
            for _r_val in [-1, 4, 5, 24, 25]:
                self.rewards_dist.update([ep_no, i, _r_val, (rew == _r_val).sum()])

        viz_rewards = self.viz_rewards
        viz_steps = self.viz_steps

        ep_rewards = torch.stack(self.ep_rewards, 0).sum(0).squeeze(0)
        ep_steps = torch.stack(self.ep_steps, 0).sum(0).squeeze(0)

        viz_rewards.add_(ep_rewards)
        viz_steps.add_(ep_steps)

        self.ep_rewards.clear()
        self.ep_steps.clear()

        self.mean_r_step.update(
            torch.mean(ep_rewards.float() / ep_steps.float()),
            index=ep_no
        )

        end_episode_time = time.time()
        self.episode_time.update(end_episode_time - self.start_episode_time,
                                 index=ep_no)
        self.start_episode_time = time.time()

        report_freq = self.report_freq
        clients_no = self.clients_no

        if ep_no % report_freq == 0:
            batch_mean_reward = torch.sum(viz_rewards) / report_freq
            game_mean_reward = batch_mean_reward / clients_no
            r_step = torch.mean(viz_rewards.float() / viz_steps.float())

            if game_mean_reward > self.max_freq_r:
                self.max_freq_r = game_mean_reward
                self.max_freq_r_ep = ep_no

            self.agent.model_utils.save_model(r_step, game_mean_reward,
                                              ep_no, save_only_min=False)

            print_info(
                "Ep: {:d} | batch_avg_R: {:.4f} | game_avg_R: {:.4f} "
                "| R_step: {:.4f} | (Max_R: {:.4f} at ep {:d})".format(
                    ep_no, batch_mean_reward, game_mean_reward,
                    r_step,
                    self.max_freq_r,
                    self.max_freq_r_ep
                )
            )
            print_info(
                "Ep: %d | (Ep_avg_time: %.4f)" % (ep_no, self.episode_time.avg)
            )
            viz_rewards.fill_(0)
            viz_steps.fill_(0)


def run_malmo_socket_server(shared_objects, cfg):

    # -- Set seed in torch and numpy

    if cfg.general.seed > 0:
        torch.manual_seed(cfg.general.seed)
        np.random.seed(cfg.general.seed)
        random.seed(cfg.general.seed)
        torch.cuda.manual_seed_all(cfg.general.seed)

    batch_size = cfg.general.batch_size
    training_eps = int(cfg.training.episodes_no / batch_size)
    server = MalmoDockerSocketServer(batch_size, shared_objects, cfg)
    server.run(training_eps)
