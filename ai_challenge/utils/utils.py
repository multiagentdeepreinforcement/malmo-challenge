# Village People, 2017

import torch
import os
import shutil
import time
import yaml
import csv
import numpy as np

def read_config():
    import yaml
    import argparse

    def to_namespace(d):
        n = argparse.Namespace()
        for k, v in d.items():
            setattr(n, k, to_namespace(v) if isinstance(v, dict) else v)
        return n

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-cf" "--config_file",
                            default="configs/basic.yaml",
                            dest="config_file",
                            help="Configuration file.")
    args = arg_parser.parse_args()
    with open(args.config_file) as f:
        config_data = yaml.load(f, Loader=yaml.SafeLoader)

    return to_namespace(config_data)

def parse_clients_args(endpoints):
    """ Return an array of tuples (ip, port) extracted from ip:port string
        :param args_clients:
        :return:
    """
    return [str.split(str(client), ':') for client in endpoints]


class SaveToCSV(object):

    def __init__(self, save_path, header):
        self.save_path = save_path
        self.save_file = open(self.save_path, 'w')
        f = self.save_file
        self.writer = csv.writer(f)
        self.writer.writerow(header)
        self.count = 0

    def update(self, vals, index=None):
        self.writer.writerow(vals)
        if self.count >= 1000:
            self.count
            self.save_file.flush()
        self.count += 1


class AverageMeter(object):
    """Computes and stores the average and current value"""
    val = avg = sum = count = None
    min = max = is_min = is_max = None
    save_path = None

    def __init__(self, keep_list=False, save_path=None):
        self.keep_list = keep_list
        self.save_path = None
        if keep_list:
            self.values = []
            self.index = []
        else:
            self.values = None
            self.index = None

        if save_path is not None:
            self.set_save_path(save_path)

        self.full_reset()

    def set_save_path(self, save_path):
        self.save_path = save_path
        self.save_file = open(self.save_path, 'w')
        f = self.save_file
        self.writer = csv.writer(f)
        self.writer.writerow(["Value", "Index"])

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def full_reset(self):
        self.min = +np.inf
        self.max = -np.inf
        self.is_min = False
        self.is_max = False

        self.reset()

        if self.keep_list:
            self.values.clear()
            self.index.clear()

    def index_group(self):
        self.index.append(len(self.values))

    def close(self):
        if self.save_path is not None:
            self.save_file.flush()
            self.save_file.close()

    def update(self, val, index=None):
        if self.keep_list:
            self.values.append(val)
            self.index.append(index)

        if self.save_path is not None:
            self.writer.writerow([val, index])
            if self.count % 500 == 0:
                self.save_file.flush()

        self.is_min = self.is_max = False
        if val < self.min:
            self.min = val
            self.is_min = True
        elif val > self.max:
            self.max = val
            self.is_max = True

        self.val = val
        self.sum += val
        self.count += 1
        self.avg = self.sum / self.count

    def update_multiple(self, val_sum, n):
        val_mean = val_sum / float(n)
        if self.keep_list:
            for i in range(n):
                self.values.append(val_mean)

        self.val = val_mean
        self.sum += val_sum
        self.count += n
        self.avg = self.sum / self.count


class ModelUtil(object):
    def __init__(self, cfg):

        save_folder_base = cfg.model.saving.save_folder
        save_folder_prefix = cfg.model.saving.save_prefix
        overwrite = cfg.model.saving.overwrite
        self.save_path = os.path.join(save_folder_base,
                                      save_folder_prefix)
        self.arch_name = cfg.model.saving.name
        self.max_r_frame = -25
        self.max_r_ep = -25

        if os.path.isdir(self.save_path):
            if overwrite:
                print("Remove previous experiment...")
                shutil.rmtree(self.save_path)
            else:
                print("New experiment - Same name...")
                prefix = "{}_{}".format(save_folder_prefix,
                                        int(time.time()*100000))
                self.save_path = os.path.join(save_folder_base,
                                           prefix)

        print("Creating checkpoints folder...")
        os.makedirs(self.save_path)
        self.checkpoint_path = os.path.join(self.save_path, "checkpoint")
        self.checkpoint_save_freq = int(cfg.model.saving.save_freq * (1024 / cfg.general.batch_size))
        self.checkpoint_next_save = 0

        # Save full config file
        with open(os.path.join(self.save_path, "config"), 'w') as f:
            yaml.dump(cfg.__dict__, f, default_flow_style=False)

        self.model = None
        self.optimizer = None

    def save_model(self, r_frame, r_ep, iteration, save_only_min=False):
        if not save_only_min and iteration >= self.checkpoint_next_save:
            self.checkpoint_next_save += self.checkpoint_save_freq
            torch.save({
                'iteration': iteration,
                'arch': self.arch_name,
                'state_dict': self.model.state_dict(),
                'optim_dict': self.optimizer.state_dict() if self.optimizer is not None else None,
                'r_frame': r_frame,
                'r_ep': r_ep,
                'max_r_ep': self.max_r_ep,
                'max_r_frame': self.max_r_frame
            }, (self.checkpoint_path + "_{}".format(iteration)))

        if r_ep > self.max_r_ep:
            self.max_r_ep = r_ep
            torch.save({
                'iteration': iteration,
                'arch': self.arch_name,
                'state_dict': self.model.state_dict(),
                'optim_dict': self.optimizer.state_dict() if self.optimizer is not None else None,
                'r_frame': r_frame,
                'r_ep': r_ep,
                'max_r_ep': self.max_r_ep,
                'max_r_frame': self.max_r_frame
            }, (self.checkpoint_path + "_max"))

        if r_frame > self.max_r_frame:
            self.max_r_frame = r_frame


    def set_model(self, model, optimizer=None):
        self.model = model
        self.optimizer = optimizer
