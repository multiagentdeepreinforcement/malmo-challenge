"""
Kaixhin/malmo-challenge
https://github.com/Kaixhin/malmo-challenge
https://github.com/Kaixhin/malmo-challenge/blob/master/ai_challenge/pig_chase/pc_environment.py

"""

import socket
import time
import docker
import torch
from torch import multiprocessing as mp
from pig_chase.common import ENV_AGENT_NAMES
from pig_chase.agent import PigChaseChallengeAgent, RandomAgent
from pig_chase.environment import PigChaseEnvironment, \
    PigChaseSymbolicStateBuilder


# Taken from Minecraft/launch_minecraft_in_background.py
def _port_has_listener(port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex(('127.0.0.1', port))
    sock.close()
    return result == 0

class Env():
    def __init__(self, rank, agent_builder, use_cuda=False):

        self.rank = rank
        self.dtype = torch.LongTensor([])
        if use_cuda:
            self.dtype = self.dtype.cuda()

        # Start Minecraft dockers ______________________________________________
        docker_client = docker.from_env()
        agent_port, partner_port = 10000 + rank, 20000 + rank
        clients = [('127.0.0.1', agent_port), ('127.0.0.1', partner_port)]

        # Assume Minecraft launched if port has listener, launch otherwise
        if not _port_has_listener(agent_port):
            self._launch_malmo(docker_client, agent_port)
        print('Malmo running on port ' + str(agent_port))
        if not _port_has_listener(partner_port):
            self._launch_malmo(docker_client, partner_port)
        print('Malmo running on port ' + str(partner_port))
        # ______________________________________________________________________

        # Set up partner agent env in separate process _________________________
        p = mp.Process(target=self._run_partner, args=(clients,))
        p.daemon = True
        p.start()
        time.sleep(3)

        # Set up agent env _____________________________________________________
        self.env = PigChaseEnvironment(clients, agent_builder, role=1,
                                       randomize_positions=True)

    # Transform in torch tensor
    def _map_to_observation(self, observation):
        dtype = self.dtype

        state_ = torch.LongTensor(observation).type_as(dtype).unsqueeze(0)
        return state_

    def _map_env_return(self, observation, reward, done):
        dtype = self.dtype

        state_ = torch.LongTensor(observation).type_as(dtype).unsqueeze(0)
        reward_ = torch.LongTensor([int(reward)]).type_as(dtype)
        done_ = torch.LongTensor([int(done)]).type_as(dtype)
        return state_, reward_, done_

    def get_class_label(self):
        return self.agent_type.value() - 1

    def reset(self):
        observation = self.env.reset()
        while observation is None:
            print("am primit o pulaaaa")
            # May happen if episode ended with first action of other agent
            observation = self.env.reset()

        return self._map_to_observation(observation)

    def step(self, action):
        observation, reward, done = self.env.do(action)
        return self._map_env_return(observation, reward, done) +  \
               (None,) # Do not return any extra info

    def close(self):
        return  # TODO: Kill processes + Docker containers

    def _launch_malmo(self, client, port):
        # Launch Docker container
        client.containers.run('malmo', '-port ' + str(port), detach=True,
                              network_mode='host')
        # Check for port to come up
        launched = False
        for _ in range(100):
            time.sleep(3)
            if _port_has_listener(port):
                launched = True
                break
        # Quit if Malmo could not be launched
        if not launched:
            exit(1)

    # Runs partner in separate env
    def _run_partner(self, clients):
        env = PigChaseEnvironment(clients, PigChaseSymbolicStateBuilder(),
                                  role=0, randomize_positions=True)
        agent = PigChaseChallengeAgent(ENV_AGENT_NAMES[0])
        self.agent_type = type(
            agent.current_agent) == RandomAgent and \
                          PigChaseEnvironment.AGENT_TYPE_1 or \
                          PigChaseEnvironment.AGENT_TYPE_2
        print(self.agent_type)
        obs = env.reset(self.agent_type)
        # print("MALMO AGENT A* ________________\n\n", obs,
        #       "-------------------------------------------------------------")

        reward = 0
        agent_done = False
        while True:
            # Select an action
            if env.done:
                self.agent_type = type(agent.current_agent) == RandomAgent \
                                  and PigChaseEnvironment.AGENT_TYPE_1 \
                                  or PigChaseEnvironment.AGENT_TYPE_2

                obs = env.reset(self.agent_type)
                while obs is None:
                    # this can happen if the episode ended with the first
                    # action of the other agent
                    print('Warning: Challenger received obs == None. _id: {} _ '
                          .format(self.rank))
                    obs = env.reset(self.agent_type)

            action = agent.act(obs, reward, agent_done, is_training=True)

            # Take a step
            obs, reward, agent_done = env.do(action)
