import os
import glob
import argparse
import subprocess
from concurrent.futures import ThreadPoolExecutor as Pool
from functools import partial
import time
import pprint

TRAIN_SCRIPT = "python test_challenge.py {} -cf {}"
WORKING_CUDA = dict()
MAX_TRIALS_NO_FILES = 1

def callback(a, *args):
    source, config_file = a
    WORKING_CUDA[source] = False

    if len(args) <= 0:
        print("No out from callback")
    else:
        future = args[0]
        if future.exception() is not None:
            print("got exception: %s" % future.exception())
        else:
            print("process returned %d" % future.result())

def to_namespace(d):
    n = argparse.Namespace()
    for k, v in d.items():
        setattr(n, k, to_namespace(v) if isinstance(v, dict) else v)
    return n


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Eval_experiments in folder.')
    parser.add_argument("folder",
                        help='Path to folder of experiments')
    parser.add_argument("-cfg", '--cfg_eval', type=str,
                        default="configs/basic_eval.yaml",
                        help='Config file used in evaluation')
    parser.add_argument('--only_max', dest='only_max', action='store_true')
    parser.add_argument("-cuda", '--cuda_devices', type=int, nargs='+',
                        default=[0],
                        help='List of cuda devices to use')

    args = parser.parse_args()
    folders = glob.glob(args.folder + "/*/")
    cfg_file = args.cfg_eval

    if os.path.isdir(cfg_file):
        cfg_file = glob.glob("{}/*.yaml".format(cfg_file))
        print(cfg_file)
    else:
        cfg_file = [cfg_file]

    only_max = args.only_max

    # -- Set working
    cuda_devices = args.cuda_devices
    for i in cuda_devices:
        WORKING_CUDA[i] = False

    callbacks = dict()
    for source in WORKING_CUDA.keys():
        callbacks[source] = partial(callback, source)

    # Get checkpoints to evaluate
    all_checkpoints = dict()

    for folder in folders:
        if only_max:
            all_checkpoints[os.path.join(folder, "checkpoint_max")] = list(cfg_file)
        else:
            chkpoints = glob.glob(folder + "/checkpoint*")
            for chk in chkpoints:
                all_checkpoints[chk] = list(cfg_file)

    pprint.pprint(all_checkpoints)
    while len(all_checkpoints) > 0:
        for k, v in WORKING_CUDA.items():
            if WORKING_CUDA[k]:
                pass

            if len(all_checkpoints) <= 0:
                break

            if not v:
                chkpoint = next(iter(all_checkpoints))
                config = all_checkpoints[chkpoint].pop()
                if len(all_checkpoints[chkpoint]) <= 0:
                    all_checkpoints.pop(chkpoint)

                WORKING_CUDA[k] = True

                pool = Pool(max_workers=1)

                f = pool.submit(subprocess.call,
                                TRAIN_SCRIPT.format(chkpoint, config), shell=True)
                f.add_done_callback(partial(callback, (k, chkpoint)))
                pool.shutdown(wait=False) # no .submit() calls after that point
                print("Started work on cuda {} with config {}".format(k,
                                                                      chkpoint))

        time.sleep(60)


