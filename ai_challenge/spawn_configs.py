import os.path

VARIABLES = {
    "batch_size": ["1024", "128"],
    # "batch_size": ["1024"],
    "seed": ["1", "2", "3", "4", "5"],
    "auxiliary_tasks": [
        ('[["next_reward", 0.1], ["next_state_depth", 0.1]]', "both"),
        ('[["next_reward", 0.1]]', "next_reward"),
        ('[["next_state_depth", 0.1]]', "next_state_depth"),
        ('[]', "none")
    ],
    "auxiliary_tweaks": [
        ('[["noise", 100], ["reinforce_a3c", no], ["prob_clamp", 0.1, 0.9]]',
         "reinforce_and_v"),
        ('[["noise", 100], ["reinforce_a3c", yes], ["prob_clamp", 0.1, 0.9]]',
         "ac"),
         (
        '[["noise", 100], ["no_critic", yes], ["reinforce_a3c", no], ["prob_clamp", 0.1, 0.9]]',
        "reinforce"),
    ],

    # "p_focus": [".0", ".25", ".5", ".75", "1."],
    # "auxiliary_tasks": [
    #    ('[["next_reward", 0.1], ["next_state_depth", 0.1], ["entropy", 0.1]]', "entropy"),
    #    ('[["next_reward", 0.1], ["next_state_depth", 0.1]]', "noEntropy")
    #],
    #"auxiliary_tweaks": [
    #    ('[["noise", 100], ["reinforce_a3c", yes], ["adjust_lr", 3000, 0.1], ["prob_clamp", 0.1, 0.9]]', "a3c_clamp"),
    #    ('[["noise", 100], ["reinforce_a3c", yes], ["adjust_lr", 3000, 0.1], ["prob_clamp", 0.001, 0.999]]', "a3c_noClamp"),
    #    ('[["noise", 100], ["reinforce_a3c", no], ["adjust_lr", 3000, 0.1], ["prob_clamp", 0.1, 0.9]]', "vp_clamp"),
    #    ('[["noise", 100], ["reinforce_a3c", no], ["adjust_lr", 3000, 0.1], ["prob_clamp", 0.001, 0.999]]', "vp_noClamp")
    #],

    # "batch_size": ["128"],
    # "seed": ["1", "2", "3", "4", "5"],
    # "p_focus": [".0", ".25", ".5", ".75", "1."],
    # "auxiliary_tasks": [
    #     ('[["next_reward", 0.1], ["next_state_depth", 0.1], ["entropy", 0.1]]', "entropy"),
    #     ('[["next_reward", 0.1], ["next_state_depth", 0.1]]', "noEntropy")
    # ],
    # "auxiliary_tasks": [
    #     ('[["next_reward", 0.1], ["next_state_depth", 0.1], ["entropy", 0.1]]', "entropy"),
    #     ('[["next_reward", 0.1], ["next_state_depth", 0.1]]', "noEntropy")
    # ],
    # "auxiliary_tweaks": [
    #     ('[["noise", 100], ["reinforce_a3c", yes], ["prob_clamp", 0.1, 0.9]]', "a3c_clamp"),
    #     ('[["noise", 100], ["reinforce_a3c", yes], ["prob_clamp", 0.001, 0.999]]', "a3c_noClamp"),
    #     ('[["noise", 100], ["reinforce_a3c", no], ["adjust_lr", 3000, 0.1], ["prob_clamp", 0.1, 0.9]]', "vp_clamp"),
    #     ('[["noise", 100], ["reinforce_a3c", no], ["adjust_lr", 3000, 0.1], ["prob_clamp", 0.001, 0.999]]', "vp_noClamp")
    # ],
    # "auxiliary_tweaks": [
    #     ('[["noise", 100], ["reinforce_a3c", no], ["adjust_lr", 3000, 0.1], ["prob_clamp", 0.1, 0.9]]', "std"),
    #     ('[["noise", 100], ["reinforce_a3c", no], ["adjust_lr", 3000, 0.1], ["prob_clamp", 0.1, 0.9], ["no_critic", yes], ["no_final_bn", yes]]', "no_critic")
    # ]
    # "auxiliary_tweaks": [
    #    ('[["noise", 100], ["reinforce_a3c", no], ["prob_clamp", 0.1, 0.9]]', "std"),
    #    ('[["noise", 100], ["reinforce_a3c", no], ["prob_clamp", 0.1, 0.9], ["no_final_bn", yes]]', "no_bn")
    # ]
    # "auxiliary_tweaks": [
    #    ('[["noise", 100], ["reinforce_a3c", no], ["prob_clamp", 0.00001, 0.99999]]', "clamp_1e05"),
    #    ('[["noise", 100], ["reinforce_a3c", no], ["prob_clamp", 0.2, 0.8]]', "clamp_2e"),
    #    ('[["noise", 100], ["reinforce_a3c", no], ["prob_clamp", 0.3, 0.7]]', "clamp_3e")
    # ]
}


def generate_files(template_name):
    l = len(VARIABLES)
    var_names = list(VARIABLES.keys())

    print("{:d} variables to combine".format(l))

    stack = [-1]
    k = 0

    folder = os.path.join("pools", template_name)
    os.mkdir(folder)

    while k >= 0:
        stack[k] += 1

        if stack[k] == len(VARIABLES[var_names[k]]):
            stack.pop()
            k -= 1
        elif k == (l - 1):
            values = [VARIABLES[var_names[j]][stack[j]] for j in range(k+1)]
            parts = [(n, v if not isinstance(v, tuple) else v[1]) for (n, v) in zip(var_names, values) if n != "seed"]
            ps = ["{:s}_{}".format(n, v) for (n, v) in parts]
            seed = values[var_names.index("seed")]
            new_name = "{:s}___{:s}__{:s}".format(
                template_name, "__".join(ps), seed
            )

            orig_yaml = template_name + ".yaml"
            with open(os.path.join("config_templates", orig_yaml)) as orig:
                with open(os.path.join(folder, new_name + ".yaml"), "w") as f:
                    line = orig.readline()
                    while line:
                        if "XXX" in line:
                            for n, v in zip(var_names, values):
                                if n in line:
                                    if isinstance(v, tuple):
                                        line = line.replace("XXX", v[0])
                                    else:
                                        line = line.replace("XXX", v)
                        elif "save_prefix" in line:
                            line = line.replace("YYY", new_name + "_")
                        assert ("XXX" not in line) and ("YYY" not in line)
                        f.write(line)
                        line = orig.readline()

        else:
            stack.append(-1)
            k += 1



def main():
    from argparse import ArgumentParser
    arg_parser = ArgumentParser()
    arg_parser.add_argument("template")
    args = arg_parser.parse_args()

    template_name = args.template + ".yaml"
    assert os.path.isfile(os.path.join("config_templates", template_name))

    experiment_name = args.template
    assert not os.path.isdir(os.path.join("pools", experiment_name))

    generate_files(args.template)

if __name__ == "__main__":
    main()
