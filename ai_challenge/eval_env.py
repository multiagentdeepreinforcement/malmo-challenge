# Village People, 2017

from env.artificial_malmo import ArificialMalmo

from env.village_env_agents import VillagePeopleEnvChallengeAgent

from env.PigChaseChallengeAgent_Replica import PigChaseChallengeAgent_V
from models import get_model
from agents import get_agent
import torch
from utils import AtomicStatistics
from pig_chase.common import ENV_ACTIONS
from env.village_env_agents import Binary18BatchAgentWrapper
import time
from termcolor import colored as clr
import os.path as path

import yaml
import argparse
import numpy as np
import random
from utils import AverageMeter
import os
import shutil
import torch.optim as optim

def print_info(message):
    print(clr("[ARTF_TRAIN] ", "magenta") + message)

def to_namespace(d):
    n = argparse.Namespace()
    for k, v in d.items():
        setattr(n, k, to_namespace(v) if isinstance(v, dict) else v)
    return n

    return to_namespace(config_data)

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("checkpoint",
                            help="Checkpoint load.")
    arg_parser.add_argument("-cf" "--config_file",
                            default="configs/basic_eval.yaml",
                            dest="config_file",
                            help="Configuration file.")
    arg_parser.add_argument("-ow" "--overwrite",
                            default=False,
                            type=bool,
                            dest="overwrite",
                            help="Overwrite eval result folder.")

    args = arg_parser.parse_args()

    with open(args.config_file) as f:
        config_data = yaml.load(f, Loader=yaml.SafeLoader)

    cfg = to_namespace(config_data)

    # _EVAL MODE MUST LOAD A CHECKPOINT
    if args.checkpoint is not None:
        cfg.model.load = args.checkpoint
    if not os.path.isfile(cfg.model.load):
        assert False, "No checkpoint set"

    # _EVAL RESULTS FOLDER______________________________________________________
    overwrite = args.overwrite
    cfg.model.saving.overwrite = overwrite
    cfg.model.saving.save_folder = os.path.dirname(cfg.model.load)
    cfg.model.saving.save_prefix = os.path.basename(cfg.model.load) + \
                                   "_{}".format(cfg.model.saving.save_prefix) +\
                                   "_eval"

    if cfg.general.seed > 0:
        torch.manual_seed(cfg.general.seed)
        np.random.seed(cfg.general.seed)
        random.seed(cfg.general.seed)
        torch.cuda.manual_seed_all(cfg.general.seed)

    batch_size = cfg.general.batch_size

    # -- Initialize simulated environment
    env = ArificialMalmo(cfg.envs.simulated)
    print_info("Environment initialized (batch_size:={:d}).".format(batch_size))

    # -- Configure model
    model = get_model(cfg.model.name)(cfg.model)
    if cfg.general.use_cuda:
        model.cuda()

    Optimizer = getattr(optim, cfg.training.algorithm)
    optim_args = vars(cfg.training.algorithm_args)
    optimizer = Optimizer(model.parameters(), **optim_args)

    if isinstance(cfg.model.load, str):
        checkpoint = torch.load(cfg.model.load)
        if "max_r_ep" in checkpoint:
            reward = checkpoint['max_r_ep']
        else:
            reward = checkpoint['reward']
        print("Loading Model: {} Mean reward/ episode: {}"
              "".format(cfg.model.load, reward))
        model.load_state_dict(checkpoint['state_dict'])
        if "optim_dict" in checkpoint:
            optimizer.load_state_dict(checkpoint['optim_dict'])

    print_info("Model {:s} initalized.".format(clr(cfg.model.name, "red")))

    # -- Shared objects
    objects = {
        "model": model,
        "optimizer": optimizer
    }

    # -- Initialize alien
    alien = VillagePeopleEnvChallengeAgent(PigChaseChallengeAgent_V,
                                           cfg.alien.name,
                                           env._board_one_hot,
                                           cfg)

    # -- Initialize agent and wrap it in a Binary18BatchAgentWrapper :)
    Agent = get_agent(cfg.agent.type)
    agent = Agent(cfg.agent.name, ENV_ACTIONS, cfg, objects)
    agent_runner = Binary18BatchAgentWrapper(agent, cfg.agent.name, cfg,
                                             is_training=False)

    print_info("{:s}<{:s}> agent is up and waiting to learn. |role={:d}".format(
        cfg.agent.name, cfg.agent.type, cfg.agent.role
    ))

    print_info("Alien is up.")
    # - get alien type [-TorchTensor-]
    alien_type = cfg.agents_type

    # -- Start training
    agents = [alien, agent_runner]
    agent_idx = 1

    env_agents_data = [env.agent0, env.agent1]

    dtype = torch.LongTensor(0)
    if cfg.general.use_cuda:
        dtype = dtype.cuda()

    def restartGame():
        obs = env.reset()
        reward = torch.zeros(batch_size).type_as(dtype)
        done = torch.zeros(batch_size).type_as(dtype)

        for agent in agents:
            agent.reset()

        return obs, reward, done

    obs, reward, done = restartGame()
    ep_cnt = 0
    crt_agent = 0

    # Batch of agents used for evaluation during training.
    eval_agents_count = batch_size
    if cfg.evaluation.during_training.truncate:
        eval_agents_count = int(batch_size * cfg.agent.exploration[0][1])

    ep_rewards = torch.LongTensor(eval_agents_count).type_as(dtype)
    ep_steps = torch.LongTensor(eval_agents_count).type_as(dtype)

    ep_rewards.fill_(0)
    ep_steps.fill_(0)

    start_time = time.time()
    episode_time = AverageMeter()

    save_path = agent.model_utils.save_path
    print("____________{}".format(save_path))
    mean_reward_save = AverageMeter(save_path=path.join(save_path,
                                                    "mean_r_game_ep"))
    mean_r_step = AverageMeter(save_path=path.join(save_path,
                                                    "mean_r_step_ep"))
    mean_reward_save_R = AverageMeter(save_path=path.join(save_path,
                                                    "mean_r_game_ep_RANDOM"))
    mean_r_step_R = AverageMeter(save_path=path.join(save_path,
                                                    "mean_r_step_ep_RANDOM"))
    mean_reward_save_A = AverageMeter(save_path=path.join(save_path,
                                                    "mean_r_game_ep_ASTAR"))
    mean_r_step_A = AverageMeter(save_path=path.join(save_path,
                                                    "mean_r_step_ep_ASTAR"))
    a_star_games_count = AverageMeter(save_path=path.join(save_path,
                                                    "game_count_ASTAR"))
    games_alive = AverageMeter(save_path=path.join(save_path,
                                                    "games_alive"))

    # CALCULATE FOR EACH EPISODE _______________________________________________
    report_freq = 1

    print_info("No of epochs: {:d}. Max no of steps/epoch: {:d}".format(
        cfg.training.episodes_no, cfg.training.max_step_no
    ))

    training_eps = int(cfg.training.episodes_no / batch_size)

    start_episode_time = time.time()
    start_report_time = time.time()

    max_freq_r = -100
    max_freq_r_ep = -1
    step = 1
    game_step = 0
    while ep_cnt < training_eps:
        step += 1
        # check if env needs reset
        if env.done.all():
            game_step = 0
            # REPORT FREQ SET PERMANANT TO FREQ 1:
            batch_mean_reward = torch.sum(ep_rewards)
            game_mean_reward = batch_mean_reward / eval_agents_count
            r_step = torch.mean(ep_rewards.float() / ep_steps.float())

            last_report_time = time.time() - start_report_time
            start_report_time = time.time()

            if game_mean_reward > max_freq_r:
                max_freq_r = game_mean_reward
                max_freq_r_ep = ep_cnt

            # Save data for eval
            mean_reward_save.update(game_mean_reward, index=ep_cnt)
            mean_r_step.update(r_step,
                               index=ep_cnt)

            # Calculate score only for A*
            astar_mask = alien_type
            astar_count = torch.sum(astar_mask)
            if astar_count != 0:
                a_ep_rewards = ep_rewards.masked_select(astar_mask.byte())
                a_ep_steps = ep_steps.masked_select(astar_mask.byte())
                a_game_mean_reward = torch.sum(a_ep_rewards) / astar_count
                a_r_step = torch.mean(a_ep_rewards.float() / a_ep_steps.float())
                mean_reward_save_A.update(a_game_mean_reward, index=ep_cnt)
                mean_r_step_A.update(a_r_step, index=ep_cnt)
            else:
                mean_reward_save_A.update(0, index=ep_cnt)
                mean_r_step_A.update(0, index=ep_cnt)

            # Calculate score only for Random
            random_mask = 1-alien_type
            random_count = torch.sum(random_mask)
            if random_count != 0:
                r_ep_rewards = ep_rewards.masked_select(random_mask.byte())
                r_ep_steps = ep_steps.masked_select(random_mask.byte())
                r_game_mean_reward = torch.sum(r_ep_rewards) / random_count
                r_r_step = torch.mean(r_ep_rewards.float() / r_ep_steps.float())
                mean_reward_save_R.update(r_game_mean_reward, index=ep_cnt)
                mean_r_step_R.update(r_r_step, index=ep_cnt)
            else:
                mean_reward_save_R.update(0, index=ep_cnt)
                mean_r_step_R.update(0, index=ep_cnt)

            episode_time.update(time.time() - start_episode_time)
            start_episode_time = time.time()

            obs, reward, done = restartGame()
            crt_agent = 0

            print_info("Ep: %d | batch_avg_R: %.4f | game_avg_R: %.4f "
                       "| R_step: %.4f | (Max_R: %.4f at ep %d)" %
                       (ep_cnt, batch_mean_reward, game_mean_reward, r_step,
                        max_freq_r, max_freq_r_ep))
            print_info(
                "Ep: %d | (Ep_avg_time: %.4f) | (Last_report: %.4f)" %
                (ep_cnt, episode_time.avg, last_report_time))

            ep_rewards.fill_(0)
            ep_steps.fill_(0)
            ep_cnt += 1

        # select an action
        if crt_agent == agent_idx:
            game_step += 1

        agent_act = agents[crt_agent].act(
            obs, reward, done, (1 - env_agents_data[crt_agent].got_done))

        # take a step
        obs, reward, done = env.do(agent_act)
        crt_agent = (crt_agent + 1) % 2

        if crt_agent == agent_idx:
            not_done = 1 - env.done.long()
            ep_steps.add_(not_done[:eval_agents_count])
            # -Save mean reward per alive games (each step)
            ep_rewards.add_(reward[:eval_agents_count])
            games_alive.update(torch.sum(not_done[:eval_agents_count]),
                               index=(ep_cnt, step))

    elapsed_time = time.time() - start_time
    print("Finished in %.2f seconds at %.2ffps." % (
        elapsed_time, step / elapsed_time))
