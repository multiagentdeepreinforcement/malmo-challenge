import socket
import torch

SLAVES = 8
HOST = '127.0.0.1'
PORT = 15022

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))

s.listen(16)

connections = []
for i in range(SLAVES):
    conn, _ = s.accept()
    connections.append(conn)

print("All slaves connected")

for (idx, conn) in enumerate(connections):
    msg = conn.recv(1024)
    msg = msg.decode()
    assert msg == "OK"
    conn.sendall(str.encode(str(idx)))
    msg = conn.recv(4096)
    text = msg[:2].decode()
    assert text == "OK"
    x = torch.serialization.pickle.loads(msg[2:])
    print(x)
    print(idx, "is done!")


print("All slaves sent and received messages")

for conn in connections:
    conn.close()

s.close()
