# Village People, 2017

import socket


def is_there_malmo(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((host, port))
        s.settimeout(1)
        s.recv(256)
    except socket.timeout:
        try:
            s.close()
        except:
            pass
        return True
    except:
        try:
            s.close()
        except:
            pass
        return False


def main():
    import sys
    assert len(sys.argv) == 3

    host = sys.argv[1]
    port = int(sys.argv[2])

    if is_there_malmo(host, port):
        print("yes")
    else:
        print("no")

if __name__ == "__main__":
    main()
