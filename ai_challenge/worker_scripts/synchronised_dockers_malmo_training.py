# Village People, 2017

import torch

from env.village_env_agents import Binary18BatchAgentWrapper
from pig_chase.common import ENV_ACTIONS
from utils import AverageMeter

from agents import get_agent
import socket
import time
from termcolor import colored as clr
import os.path as path
import numpy as np
import random


def print_info(message):
    print(clr("[DOCKER_MASTER] ", "magenta") + message)


class MasterDockerEnv(object):

    def __init__(self, slaves_no, cfg):
        self.slaves_no = slaves_no
        self.connections = connections = []
        self.sock = sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.HOST = HOST = cfg.host
        self.PORT = PORT = cfg.port
        self.use_cuda = cfg.use_cuda

        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        sock.bind((HOST, PORT))
        sock.listen(slaves_no)

        for ionica in range(slaves_no):
            print("Astept pe ", ionica)
            conn, addr = sock.accept()
            connections.append(conn)

            print("M-a contactat ", addr)

            conn.sendall(str.encode(str(ionica)))
            msg = conn.recv(64)
            bulica = int(msg.decode())
            assert bulica == ionica

        print("All slaves are connected!")

        self.done = torch.ByteTensor(slaves_no).fill_(0)
        self.prev_done = torch.ByteTensor(slaves_no).fill_(0)
        self.rewards = torch.LongTensor(slaves_no).fill_(0)
        self.states = torch.FloatTensor(slaves_no, 18, 9, 9)

    def close(self):
        for c in self.connections:
            c.sendall(str.encode("FINISH"))
            c.close()

    def reset(self):
        connections = self.connections
        states = self.states

        for i, c in enumerate(connections):
            try:
                c.sendall(str.encode("RESET"))
            except Exception as e:
                self.wait_new_client_and_reset_all(i)
                return self.reset()

        self.done.fill_(0)
        self.prev_done = self.done.clone()
        self.rewards.fill_(0)

        states.fill_(.0)
        for (idx, conn) in enumerate(connections):
            try:
                msg = conn.recv(4096)
                assert msg[0] == 80  # P
                data = msg[1:]
                s = torch.serialization.pickle.loads(data)["s"]
                assert s.size() == torch.Size([1, 18, 9, 9])

                states[idx].copy_(s)
            except Exception as e:
                self.wait_new_client_and_reset_all(idx)
                return self.reset()

        if self.use_cuda:
            return states.cuda()
        else:
            return states

    def wait_new_client_and_reset_all(self, ionica):
        try:
            self.connections[ionica].close()
        except Exception as e:
            pass

        print("Astept pe ", ionica)
        conn, addr = self.sock.accept()
        self.connections[ionica] = conn
        print("M-a contactat ", addr)
        conn.sendall(str.encode(str(ionica)))
        msg = conn.recv(64)
        bulica = int(msg.decode())
        assert bulica == ionica

        return "RESET"



    def do(self, actions):
        slaves_no = self.slaves_no
        connections = self.connections
        assert actions.size() == torch.Size([slaves_no])


        states = self.states
        rewards = self.rewards
        rewards.fill_(0)
        done = self.done
        self.prev_done = self.done.clone()

        for (idx, conn) in enumerate(connections):
            if not done[idx]:
                try:
                    conn.sendall(str.encode(str(actions[idx])))
                    msg = conn.recv(4096)
                    if msg[0] == 68:   # 'D'
                        done[idx] = 1
                        rewards[idx] = int(msg[1:].decode())
                    elif msg[0] == 80:   # 'P'
                        msg = msg[1:]
                        data = torch.serialization.pickle.loads(msg)
                        assert type(data) == dict and "r" in data and "s" in data
                        rewards[idx] = data["r"][0]
                        assert data["s"].size() == torch.Size([1, 18, 9, 9])
                        states[idx].copy_(data["s"])
                    else:
                        assert False, "This shoudl not happen"
                except Exception as e:
                    return self.wait_new_client_and_reset_all(idx)

        if self.use_cuda:
            return states.cuda(), rewards.cuda(), done.cuda()
        else:
            return states.clone(), rewards.clone(), done.clone()

    def get_previously_finished_games(self):
        return (1 - self.prev_done).cuda() if self.use_cuda else (1 - self.prev_done)


    def get_alive_no(self):
        return (1 - self.done).nonzero().nelement()

    def get_alive_games(self):
        if self.use_cuda:
            return (1 - self.done).long().cuda()
        else:
            return (1 - self.done).long()

    def are_all_done(self):
        return self.done.all()


def train_from_dockers(objects, cfg):

    if cfg.general.seed > 0:
        torch.manual_seed(cfg.general.seed)
        np.random.seed(cfg.general.seed)
        random.seed(cfg.general.seed)
        torch.cuda.manual_seed_all(cfg.general.seed)

    batch_size = cfg.general.batch_size
    stats = objects["stats_leRMS"]

    env = MasterDockerEnv(batch_size, cfg.docker)

    print_info("Environment initialized (slaves:={:d}).".format(batch_size))


    # -- Initialize agent and wrap it in a Binary18BatchAgentWrapper :)
    Agent = get_agent(cfg.agent.type)
    agent = Agent(cfg.agent.name, ENV_ACTIONS, cfg, objects)
    agent_runner = Binary18BatchAgentWrapper(agent, cfg.agent.name, cfg)

    print_info("{:s}<{:s}> agent is up and waiting to learn. |role={:d}".format(
        cfg.agent.name, cfg.agent.type, cfg.agent.role
    ))


    dtype = torch.LongTensor(0)
    if cfg.general.use_cuda:
        dtype = dtype.cuda()

    def restartGame():
        obs = env.reset()
        reward = torch.zeros(batch_size).type_as(dtype)
        done = torch.zeros(batch_size).type_as(dtype)
        agent_runner.reset()
        return obs, reward, done

    obs, reward, done = restartGame()
    ep_cnt = 0
    crt_step = 0


    # Batch of agents used for evaluation during training.
    eval_agents_count = batch_size
    if cfg.evaluation.during_training.truncate:
        eval_agents_count = int(batch_size * cfg.agent.exploration[0][1])

    viz_rewards = torch.LongTensor(eval_agents_count).type_as(dtype)
    viz_steps = torch.LongTensor(eval_agents_count).type_as(dtype)
    ep_rewards = torch.LongTensor(eval_agents_count).type_as(dtype)
    ep_steps = torch.LongTensor(eval_agents_count).type_as(dtype)

    viz_rewards.fill_(0)
    viz_steps.fill_(0)
    ep_rewards.fill_(0)
    ep_steps.fill_(0)

    start_time = time.time()
    episode_time = AverageMeter(save_path=path.join(agent.model_utils.save_path,
                                                    "episode_time"))
    reward_save = AverageMeter(save_path=path.join(agent.model_utils.save_path,
                                                    "reward_sum_step"))
    games_alive = AverageMeter(save_path=path.join(agent.model_utils.save_path,
                                                    "games_alive"))
    mean_r_step = AverageMeter(save_path=path.join(agent.model_utils.save_path,
                                                    "mean_r_step"))
    report_freq = cfg.general.report_freq

    print_info("No of epochs: {:d}. Max no of steps/epoch: {:d}".format(
        cfg.training.episodes_no, cfg.training.max_step_no
    ))

    training_eps = int(cfg.training.episodes_no / batch_size)

    start_episode_time = time.time()
    start_report_time = time.time()

    max_freq_r = -100
    max_freq_r_ep = -1
    step = 1
    error_happened = False
    while ep_cnt < training_eps:
        step += 1
        # check if env needs reset
        if error_happened:
            ep_rewards.fill_(0)
            ep_steps.fill_(0)
            viz_rewards.fill_(0)
            viz_steps.fill_(0)
            start_episode_time = time.time()
            obs, reward, done = restartGame()
            error_happened = False
            crt_step = 0
        elif env.are_all_done():

            agent_runner.act(
                obs, reward, done, env.get_previously_finished_games())

            viz_rewards.add_(ep_rewards)
            viz_steps.add_(ep_steps)

            mean_r_step.update(torch.mean(ep_rewards.float()/ ep_steps.float()),
                               index=ep_cnt)
            ep_rewards.fill_(0)
            ep_steps.fill_(0)

            episode_time.update(time.time() - start_episode_time,
                                index=(ep_cnt, step))
            start_episode_time = time.time()

            obs, reward, done = restartGame()
            ep_cnt += 1
            stats.inc_episodes(batch_size)

            if ep_cnt % report_freq == 0:
                batch_mean_reward = torch.sum(viz_rewards) / report_freq
                game_mean_reward = batch_mean_reward / eval_agents_count
                last_report_time = time.time() - start_report_time
                start_report_time = time.time()
                r_step = torch.mean(viz_rewards.float() / viz_steps.float())

                if game_mean_reward > max_freq_r:
                    max_freq_r = game_mean_reward
                    max_freq_r_ep = ep_cnt
                agent.model_utils.save_model(r_step, game_mean_reward,
                                             ep_cnt, save_only_min=False)

                print_info("Ep: %d | batch_avg_R: %.4f | game_avg_R: %.4f "
                           "| R_step: %.4f | (Max_R: %.4f at ep %d)" %
                           (ep_cnt, batch_mean_reward, game_mean_reward, r_step,
                            max_freq_r, max_freq_r_ep))
                print_info(
                    "Ep: %d | (Ep_avg_time: %.4f) | (Last_report: %.4f)" %
                    (ep_cnt, episode_time.avg, last_report_time))
                viz_rewards.fill_(0)
                viz_steps.fill_(0)
            crt_step = 0

        # select an action
        agent_act = agent_runner.act(
            obs, reward, done, env.get_previously_finished_games())
        stats.inc_frames(env.get_alive_no())

        # take a step
        result = env.do(agent_act)

        if result == "RESET":
            agent_runner.forget_current_batch()
            error_happened = True
            reward_save.update((-100 - crt_step),
                               index=(ep_cnt, step))
        else:
            crt_step += 1
            obs, reward, done = result

            not_done = env.get_alive_games()

            ep_steps.add_(not_done[:eval_agents_count])
            # -Save mean reward per alive games (each step)
            reward_save.update(torch.sum(reward[:eval_agents_count]),
                               index=(ep_cnt, step))
            games_alive.update(torch.sum(not_done[:eval_agents_count]),
                               index=(ep_cnt, step))
            ep_rewards.add_(reward[:eval_agents_count])

    env.close()
    elapsed_time = time.time() - start_time
    print("Finished in %.2f seconds at %.2ffps." % (
        elapsed_time, step / elapsed_time))
