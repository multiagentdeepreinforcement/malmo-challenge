import sys

from worker_scripts.malmo_docker_socket_client import \
    run_malmo_docker_socket_client

if __name__ == "__main__":

    # -- This runs in two modes:
    #    launch_docker_socket_client.py server_host server_port my_id
    #    launch_docker_socket_client.py server_host server_port my_id ip1 ip2

    assert len(sys.argv) == 4 or len(sys.argv) == 6

    server_host = sys.argv[1]
    server_port = int(sys.argv[2])
    server_info = (server_host, server_port)

    my_id = int(sys.argv[3])

    if len(sys.argv) == 6:
        player_host = sys.argv[4]
        challenger_host = sys.argv[5]
        clients = [(player_host, 10000), (challenger_host, 10000)]
    else:
        clients = None

    print("[MAIN] Starting ", my_id)
    run_malmo_docker_socket_client(clients, server_info, my_id)
