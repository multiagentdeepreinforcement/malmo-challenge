# Village People, 2017

import torch

from env.artificial_malmo import ArificialMalmo
from env.artificial_malmo import VillagePeopleEnvAgent
from env.village_env_agents import Binary18BatchAgentWrapper
from env.village_env_agents import MalmoAgentWrapper
from pig_chase.common import ENV_ACTIONS
from pig_chase.agent import PigChaseChallengeAgent
from env.PigChaseChallengeAgent_Replica import PigChaseChallengeAgent_V
from utils import AverageMeter
from env.village_env_agents import VillagePeopleEnvChallengeAgent

from agents import get_agent

import time
from termcolor import colored as clr
import os.path as path
import numpy as np
import random

def print_info(message):
    print(clr("[ARTF_TRAIN] ", "magenta") + message)


# Train on our environment
def train_on_simulator(shared_objects, cfg):

    if cfg.general.seed > 0:
        torch.manual_seed(cfg.general.seed)
        np.random.seed(cfg.general.seed)
        random.seed(cfg.general.seed)
        torch.cuda.manual_seed_all(cfg.general.seed)

    batch_size = cfg.general.batch_size


    # -- Initialize simulated environment
    env = ArificialMalmo(cfg.envs.simulated)
    print_info("Environment initialized (batch_size:={:d}).".format(batch_size))

    # -- Initialize alien
    alien = VillagePeopleEnvChallengeAgent(PigChaseChallengeAgent_V,
                                           cfg.alien.name,
                                           env._board_one_hot,
                                           cfg)
    print_info("Alien is up.")


    # -- Initialize agent and wrap it in a Binary18BatchAgentWrapper :)
    Agent = get_agent(cfg.agent.type)
    agent = Agent(cfg.agent.name, ENV_ACTIONS, cfg, shared_objects)
    agent_runner = Binary18BatchAgentWrapper(agent, cfg.agent.name, cfg)

    print_info("{:s}<{:s}> agent is up and waiting to learn. |role={:d}".format(
        cfg.agent.name, cfg.agent.type, cfg.agent.role
    ))

    # -- Start training
    agents = [alien, agent_runner]
    agent_idx = 1

    env_agents_data = [env.agent0, env.agent1]

    dtype = torch.LongTensor(0)
    if cfg.general.use_cuda:
        dtype = dtype.cuda()

    def restartGame():
        obs = env.reset()
        reward = torch.zeros(batch_size).type_as(dtype)
        done = torch.zeros(batch_size).type_as(dtype)

        for agent in agents:
            agent.reset()
        return obs, reward, done

    obs, reward, done = restartGame()
    ep_cnt = 0
    crt_agent = 0


    # Batch of agents used for evaluation during training.
    eval_agents_count = batch_size
    if cfg.evaluation.during_training.truncate:
        eval_agents_count = int(batch_size * cfg.agent.exploration[0][1])

    viz_rewards = torch.LongTensor(eval_agents_count).type_as(dtype)
    viz_steps = torch.LongTensor(eval_agents_count).type_as(dtype)
    ep_rewards = torch.LongTensor(eval_agents_count).type_as(dtype)
    ep_steps = torch.LongTensor(eval_agents_count).type_as(dtype)

    viz_rewards.fill_(0)
    viz_steps.fill_(0)
    ep_rewards.fill_(0)
    ep_steps.fill_(0)

    start_time = time.time()
    episode_time = AverageMeter(save_path=path.join(agent.model_utils.save_path,
                                                    "episode_time"))
    reward_save = AverageMeter(save_path=path.join(agent.model_utils.save_path,
                                                    "reward_sum_step"))
    games_alive = AverageMeter(save_path=path.join(agent.model_utils.save_path,
                                                    "games_alive"))
    mean_r_step = AverageMeter(save_path=path.join(agent.model_utils.save_path,
                                                    "mean_r_step"))
    report_freq = cfg.general.report_freq

    print_info("No of epochs: {:d}. Max no of steps/epoch: {:d}".format(
        cfg.training.episodes_no, cfg.training.max_step_no
    ))

    training_eps = int(cfg.training.episodes_no / batch_size)

    start_episode_time = time.time()
    start_report_time = time.time()

    max_freq_r = -100
    max_freq_r_ep = -1
    step = 1
    while ep_cnt < training_eps:
        step += 1
        # check if env needs reset
        if env.done.all():
            viz_rewards.add_(ep_rewards)
            viz_steps.add_(ep_steps)

            mean_r_step.update(torch.mean(ep_rewards.float()/ ep_steps.float()),
                               index=ep_cnt)
            ep_rewards.fill_(0)
            ep_steps.fill_(0)

            episode_time.update(time.time() - start_episode_time,
                                index=(ep_cnt, step))
            start_episode_time = time.time()

            obs, reward, done = restartGame()
            ep_cnt += 1

            crt_agent = 0

            if ep_cnt % report_freq == 0:
                batch_mean_reward = torch.sum(viz_rewards) / report_freq
                game_mean_reward = batch_mean_reward / eval_agents_count
                last_report_time = time.time() - start_report_time
                start_report_time = time.time()
                r_step = torch.mean(viz_rewards.float() / viz_steps.float())

                if game_mean_reward > max_freq_r:
                    max_freq_r = game_mean_reward
                    max_freq_r_ep = ep_cnt
                agent.model_utils.save_model(r_step, game_mean_reward,
                                             ep_cnt, save_only_min=False)

                print_info("Ep: %d | batch_avg_R: %.4f | game_avg_R: %.4f "
                           "| R_step: %.4f | (Max_R: %.4f at ep %d)" %
                           (ep_cnt, batch_mean_reward, game_mean_reward, r_step,
                            max_freq_r, max_freq_r_ep))
                print_info(
                    "Ep: %d | (Ep_avg_time: %.4f) | (Last_report: %.4f)" %
                    (ep_cnt, episode_time.avg, last_report_time))
                viz_rewards.fill_(0)
                viz_steps.fill_(0)

        # select an action
        agent_act = agents[crt_agent].act(
            obs, reward, done, (1 - env_agents_data[crt_agent].got_done))


        # take a step
        obs, reward, done = env.do(agent_act)
        crt_agent = (crt_agent + 1) % 2

        if crt_agent == agent_idx:
            not_done = 1 - env.done.long()
            ep_steps.add_(not_done[:eval_agents_count])
            # -Save mean reward per alive games (each step)
            reward_save.update(torch.sum(reward[:eval_agents_count]),
                               index=(ep_cnt, step))
            games_alive.update(torch.sum(not_done[:eval_agents_count]),
                               index=(ep_cnt, step))
            ep_rewards.add_(reward[:eval_agents_count])

    elapsed_time = time.time() - start_time
    print("Finished in %.2f seconds at %.2ffps." % (
        elapsed_time, step / elapsed_time))
