# Copyright (c) 2017 Microsoft Corporation.
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# =====================================================================

from pig_chase.common import ENV_ACTIONS, ENV_AGENT_NAMES
from pig_chase.evaluation import PigChaseEvaluator
from pig_chase.environment import PigChaseEnvironment

from env.VillagePeopleBuilders import PigChaseVillagePeopleBuilder18Binary, \
    PigChaseVillagePopleAgent, PigChaseVillagePopleAgentGUI

from models import get_model
from utils import read_config
from agents import get_agent
import torch
from utils import AtomicStatistics
import os
import shutil
import time
import argparse
import yaml
import argparse


def to_namespace(d):
    n = argparse.Namespace()
    for k, v in d.items():
        setattr(n, k, to_namespace(v) if isinstance(v, dict) else v)
    return n


if __name__ == '__main__':

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("checkpoint",
                            help="Checkpoint load.")
    arg_parser.add_argument("-cf" "--config_file",
                            default="configs/basic_eval.yaml",
                            dest="config_file",
                            help="Configuration file.")

    args = arg_parser.parse_args()

    print("______________")
    print(args.config_file)
    print("______________")
    with open(args.config_file) as f:
        config_data = yaml.load(f, Loader=yaml.SafeLoader)

        config = to_namespace(config_data)

    # -- Read configuration & Load agent FORCE BATCH == 1
    config.model.load = args.checkpoint
    config.general.batch_size = 1
    config.general.use_cuda = False



    shared_model = get_model(config.model.name)(config.model)
    shared_model.eval()
    print(shared_model)

    Optimizer = getattr(torch.optim, config.training.algorithm)
    optim_args = vars(config.training.algorithm_args)
    optimizer = Optimizer(shared_model.parameters(), **optim_args)

    if isinstance(config.model.load, str):
        checkpoint = torch.load(config.model.load)
        iteration = checkpoint['iteration']
        if "max_r_ep" in checkpoint:
            reward = checkpoint['max_r_ep']
            reward_r_frame = checkpoint['max_r_frame']
        else:
            reward = checkpoint['reward']

        print("Loading Model: {} Mean reward/ episode: {}"
              "".format(config.model.load, reward))
        shared_model.load_state_dict(checkpoint['state_dict'])

    shared_model.eval()
    shared_objects = {
        "model": shared_model,
        "optimizer": optimizer,
        "stats_leRMS": AtomicStatistics()
    }

    agent_actor = get_agent(config.agent.type)(config.agent.name,
                                               ENV_ACTIONS, config,
                                               shared_objects)

    agent_role = 1

    agent = PigChaseVillagePopleAgentGUI(ENV_AGENT_NAMES[agent_role],
                                      len(ENV_ACTIONS),
                                      agent_actor,
                                      use_cuda=config.general.use_cuda)

    clients = [('127.0.0.1', 10000), ('127.0.0.1', 10001)]

    eval = PigChaseEvaluator(clients, agent, agent,
                             PigChaseVillagePeopleBuilder18Binary(agent_role))

    # --- save
    config.model.saving.save_folder = os.path.dirname(config.model.load)
    config.model.saving.save_prefix = os.path.basename(config.model.load) + \
                                   "_eval_malmo"

    save_folder_base = os.path.dirname(config.model.load)
    save_folder_prefix = config.model.saving.save_prefix
    overwrite = config.model.saving.overwrite
    save_path = os.path.join(save_folder_base,
                                  save_folder_prefix)

    if os.path.isdir(save_path):
        if overwrite:
            print("Remove previous experiment...")
            shutil.rmtree(save_path)
        else:
            print("New experiment - Same name...")
            prefix = "{}_{}".format(save_folder_prefix,
                                    int(time.time() * 100000))
            save_path = os.path.join(save_folder_base,
                                          prefix)
    os.makedirs(save_path)

    eval.run(save_path)

    print(save_path)
    # eval.save('My Exp 1', 'pig_chase_results.json')
    eval.save(os.path.dirname(config.model.load), save_path)
