"""
Run all .yaml config files from a folder (running as a pool system)
"""
TRAIN_SCRIPT = "CUDA_VISIBLE_DEVICES={} python async_train.py -cf {}"
WORKING_CUDA = dict()
MAX_TRIALS_NO_FILES = 1

import os
import glob
import argparse
import subprocess
from concurrent.futures import ThreadPoolExecutor as Pool
from functools import partial
import time
import shutil

def callback(a, *args):
    source, config_file = a
    WORKING_CUDA[source] = False

    if len(args) <= 0:
        print("No out from callback")
    else:
        future = args[0]
        if future.exception() is not None:
            print("got exception: %s" % future.exception())
        else:
            print("process returned %d" % future.result())

    os.remove(config_file)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument("pool_folder",
                        help='Path to folder of pool configs')
    parser.add_argument("-cuda", '--cuda_devices', type=int, nargs='+',
                        default=[0, 1],
                        help='List of cuda devices to use')

    args = parser.parse_args()
    pool_folder = "{}/*.yaml".format(args.pool_folder)
    cuda_devices = args.cuda_devices
    for i in cuda_devices:
        WORKING_CUDA[i] = False

    callbacks = dict()
    for source in WORKING_CUDA.keys():
        callbacks[source] = partial(callback, source)

    done_configs = []
    no_files_try = 0
    while True:
        configs = glob.glob(pool_folder)
        configs.sort(reverse=True)

        if len(configs) > 0:
            no_files_try = 0
        else:
            no_files_try += 1

        for k, v in WORKING_CUDA.items():
            if WORKING_CUDA[k]:
                pass

            if len(configs) <= 0:
                break

            if not v:
                config_file = configs.pop()
                done_configs.append(config_file)
                working_on = config_file + "_working_{}".format(k)
                shutil.move(config_file, working_on)
                WORKING_CUDA[k] = True

                pool = Pool(max_workers=1)
                f = pool.submit(subprocess.call,
                                TRAIN_SCRIPT.format(k, working_on), shell=True)
                f.add_done_callback(partial(callback, (k, working_on)))
                pool.shutdown(wait=False) # no .submit() calls after that point
                print("Started work on cuda {} with config {}".format(k,
                                                                      working_on))

        if no_files_try < MAX_TRIALS_NO_FILES:
            time.sleep(300)
        else:
            break

    print("FINISHED ALL FILES")

