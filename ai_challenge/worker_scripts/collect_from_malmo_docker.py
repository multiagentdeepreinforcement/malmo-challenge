# Village People, 2017

from env.env_docker import Env

from env.VillagePeopleBuilders import PigChaseVillagePeopleBuilder18Binary
import socket
import torch
import time
from numpy import mean

from termcolor import colored as clr

from synchronised_episodic_tcp_players import NonProtocolException


def collect_from_malmo_docker(HOST, PORT, _id):
    """This function never ends!!! You have to kill it yourself!"""
    max_ep_length = 25
    agent_role = 1
    _agent_builder = PigChaseVillagePeopleBuilder18Binary(agent_role)
    env = Env(_id, _agent_builder)
    print("<", _id, "> Env is up... dockers are connected")

    while True:

        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((HOST, PORT))

            print("M-am conectat la un gog!")

            action, reward, done, ep_length = 0, 0, False, 0
            episode = 0


            obs = env.reset()

            msg = s.recv(64)
            msg = msg.decode()

            my_client_id = int(msg)

            s.sendall(str.encode(str(my_client_id)))

            def my_print(*args):
                print("[%d]".format(my_client_id), *args)

            my_print("Connected to a new server and ready to play pig")

            reset_times = []
            reset_idx = 0

            # --- Playing starts

            msg = s.recv(1024)
            msg = msg.decode()

            assert msg == "RESET"

            reset = False

            while True:
                if done or reset:
                    if done:
                        print("Finished episode {} of length {}".format(
                            episode, ep_length
                        ))

                        msg = b"D" + str.encode(str(reward[0]))
                        s.sendall(msg)

                    # --- Wait for a new episode to begin

                        msg = s.recv(1024)
                        msg = msg.decode()
                        print(msg)
                        if msg == "FINISHED":
                            break
                        assert msg == "RESET"

                    # --- Reset and measure reset time

                    _t1 = time.time()
                    state = env.reset()
                    _t2 = time.time()
                    _t = _t2 - _t1
                    if len(reset_times) < 100:
                        reset_times.append(_t)
                    else:
                        reset_times[reset_idx] = _t
                        reset_idx = (reset_idx + 1) % 100
                    _avg_t = mean(reset_times)

                    my_print("Reset time: {:s}, Avg. reset time: {:s}".format(
                        clr("{:2.3f}".format(_t), "red" if _t > 2. else "blue"),
                        clr("{:2.3f}".format(_avg_t),
                            "red" if _avg_t > 2. else "blue")

                    ))

                    # --- Send state to server

                    assert state.size() == torch.Size([1, 18, 9, 9])

                    msg = b"P" + torch.serialization.pickle.dumps(
                        {"r": 0, "s": obs}
                    )
                    s.sendall(msg)

                    # --- Reset various local variables

                    action, reward, done, ep_length = 0, 0, False, 0
                    episode += 1

                else:
                    assert obs.size() == torch.Size([1, 18, 9, 9])
                    msg = b"P" + torch.serialization.pickle.dumps(
                        {"r": reward, "s": obs}
                    )
                    s.sendall(msg)

                # --- Receie action from server and apply it
                msg = s.recv(128).decode()
                if msg == "RESET":
                    reset = True
                else:
                    action = int(msg)
                    obs, reward, agent_done, _ = env.step(action)

                    done = (agent_done[0] == 1) or ep_length >= max_ep_length
                    ep_length += 1  # Increase episode counter

        except NonProtocolException as e:
            print("Exception with Malmo: ", clr(str(e), "red"))

            del _agent_builder
            del env
            _agent_builder = PigChaseVillagePeopleBuilder18Binary(agent_role)
            env = Env(_id, _agent_builder)



            print("<", _id, "> Env is up... dockers are connected")

        except Exception as e:
            print(e)
            msg = "No server found on {:s}:{:d}".format(HOST, PORT)
            sep = "-" * len(msg)
            print("<", _id, "> +-{:s}-+".format(sep))
            print("<", _id, "> | {:s} |".format(msg))
            print("<", _id, "> +-{:s}-+".format(sep))
            time.sleep(3)
