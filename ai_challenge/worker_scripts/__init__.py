# Village People, 2017

from .train_on_simulator import train_on_simulator
#
# # This are used in training with multiple players that collect
from worker_scripts.train_from_malmo import train_from_malmo
from worker_scripts.collect_from_malmo import collect_from_malmo
from worker_scripts.predict_for_malmo import predict_for_malmo
