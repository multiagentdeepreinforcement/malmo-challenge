#!/bin/bash

die() {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 3 ] || die "3 arguments required (dckrs no, host, port), $# provided"

echo $1 | grep -E -q '^[0-9]+$' || die "Games no. must be int, not shit:, $1"
echo $3 | grep -E -q '^[0-9]+$' || die "Port must be int, not shit: $3"

docker stop $(docker ps -qa)

for i in `seq 1 $1`; do
 z=`expr $i + 9999`
 echo "Launching on port: $z"
 docker run -d -p $z:10000 malmo:latest &
 z=`expr $z + 10000`
 echo "Launching on port: $z"
 docker run -d -p $z:10000 malmo:latest
done

m=`docker inspect -f '{{ .NetworkSettings.IPAddress }}' $(docker ps -q) | wc -l`
n=`expr 2 \* $1`

while [ $m -lt $n ]
do
echo "Having just $m out of $n"
sleep 1
m=`docker inspect -f '{{ .NetworkSettings.IPAddress }}' $(docker ps -q) | wc -l`
done

echo "Starting players too..."


ds=`docker inspect -f '{{ .NetworkSettings.IPAddress }}' $(docker ps -q)`
ds=(${ds})
size=${#ds[*]}

timestamp=`date +%s`
mkdir -p ./nohup_outputs
mkdir -p ./docker_control

echo "The id of this experiment is $timestamp. Godspeed!"

kill_script="./docker_control/"$timestamp"_kill_all.sh"
process_list="./docker_control/"$timestamp"_processes"
touch $kill_script
touch $process_list

i=0
while [ "$i" -lt "$size" ]
do
    echo "Pairing ${ds[$i]} and ${ds[$(($i+1))]}"
    o="./nohup_outputs/"$timestamp"_out_"$i
    nohup python launch_docker_socket_client.py "$2" $3 $i  ${ds[$i]} ${ds[$(($i+1))]} >$o 2>&1 &
    echo "$!" >> $process_list
    let "i=$i+2"
done

echo "process_list="$process_list >> $kill_script
cat killer.sh >> $kill_script
