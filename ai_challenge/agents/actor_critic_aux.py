# Village People, 2017

import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import numpy as np
from termcolor import colored as clr
from .agent import ReportingAgent
from models import get_model
from utils.torch_types import TorchTypes
from collections import namedtuple
import time
from utils import AverageMeter
import os.path as path
from methods import get_schedule

import math

# from termcolor import colored as clr
ENV_CAUGHT_REWARD = 25

SavedTransition = namedtuple('Transition',
                             ['obs', 'action', 'value', 'other_task',
                              'reward', 'next_obs', 'playing_idx', 'live_idx',
                              'probs'])


def ensure_shared_grads(model, shared_model):
    for param, shared_param in zip(model.parameters(),
                                   shared_model.parameters()):
        if shared_param.grad is not None:
            return
        shared_param._grad = param.grad


class VillageActorCriticAux(ReportingAgent):
    def __init__(self, name, action_space, cfg, shared_objects=None):
        # super(Village_ActorCritic_Aux, self).__init__()
        ReportingAgent.__init__(self, cfg)

        shared_objects = {} if shared_objects is None else shared_objects

        self.name = name
        self.pidx = 0
        self.actions_no = len(action_space)
        self.batch_size = batch_size = cfg.general.batch_size
        self.update_freq = batch_size
        self.hist_len = 1
        self.cuda = cfg.general.use_cuda
        self.torch_obs = True
        self.dtype = TorchTypes(self.cuda)
        self.clip_grad = cfg.training.clip_grad
        self.lr = cfg.training.algorithm_args.lr
        self.gamma = cfg.training.gamma

        self.state_dim = (18, 9, 1)

        self.id_ = np.random.randint(2)
        self.ep_training = 0
        self.step_training = 0

        if "model" in shared_objects:
            self.shared_model = shared_objects["model"]
            self.optimizer = shared_objects["optimizer"]
            self.net = shared_objects["model"]
            self.print_info("Training some shared model.")
        else:
            self.net = net = get_model(cfg.model.name)(cfg.model)
            if self.cuda:
                net.cuda()

            # TODO: initialize optimizer (not sure what's this branch for)

        self.model_utils.set_model(self.net, self.optimizer)

        self.step_cnt = 0
        self.ep_step = 0

        self.saved_transitions = []

        self._o, self._a, self._v, self._probs = None, None, None, None

        self.live_idx = None

        self.optimizer.zero_grad()

        self.hidden_state = None
        self.last_time = time.time()
        self.full_hidden_state = self.net.init_hidden(128)

        self.predict_max = True
        self.clock = 0

        self.aux_tasks = {t[0]:t[1:] for t in cfg.model.auxiliary_tasks}

        self.aux_tweaks = {t[0]:t[1:] for t in cfg.model.auxiliary_tweaks}
        if "adjust_lr" in self.aux_tweaks:
            self.adjust_lr = True
            self.adjust_lr_freq = self.aux_tweaks["adjust_lr"][0]/ batch_size
            self.adjust_lr_coef = self.aux_tweaks["adjust_lr"][1]
        else:
            self.adjust_lr = False

        if "no_critic" in self.aux_tweaks:
            self.use_critic = False
            print(clr("Do not use critic", "red"))
        else:
            self.use_critic = True

        if "prob_clamp" in self.aux_tweaks:
            self.min_prob_clamp = self.aux_tweaks["prob_clamp"][0]
            self.max_prob_clamp = self.aux_tweaks["prob_clamp"][1]
            #TODO Move hardcoded decaying clamp to config
            self.clamp_decay = get_schedule("linear", 0.4, 0.00001, 1000)
        else:
            raise NotImplementedError

        save_path = self.model_utils.save_path
        self.losses = dict()

        if not cfg.general.eval_only:
            self.entropy = AverageMeter(save_path=path.join(save_path, "entropy"))
            self.losses["total"] = AverageMeter(save_path=path.join(save_path,
                                                                    "total_loss"))
            self.losses["actor"] = AverageMeter(save_path=path.join(save_path,
                                                                    "actor_loss"))
            self.losses["critic"] = AverageMeter(save_path=path.join(save_path,
                                                                    "critic_loss"))

            for loss in self.aux_tasks.keys():
                self.losses[loss] = AverageMeter(save_path=path.join(save_path,
                                                                    "{}_loss"
                                                                     .format(loss)))

        if "agent_type" in self.aux_tasks and cfg.general.eval_only:
            self.true_agent_type = cfg.agents_type

            self.predict_agent_type = True

            self.optimizer_aux = optim.Adam(self.net.agent_type.parameters(),
                                            lr=0.0001)
            self.optimizer_aux.zero_grad()

            # ["agent_type", yes] -> Layer train mode
            loss = "agent_type"
            if self.aux_tasks[loss]:
                self.losses[loss] = [AverageMeter(save_path=path.join(save_path,
                                                                     "{}_loss"
                                                                     .format(loss))),
                                     AverageMeter(save_path=path.join(save_path,
                                                                     "{}_acc"
                                                                     .format(loss)))]

            # ["agent_type", no] -> Layer eval mode only
            else:
                self.losses[loss] = [AverageMeter(save_path=path.join(save_path,
                                                                     "{}_loss_eval"
                                                                     .format(loss))),
                                     AverageMeter(save_path=path.join(save_path,
                                                                     "{}_acc_eval"
                                                                     .format(loss)))]
        else:
            self.predict_agent_type = False

        self.optimized_ = False

        self.reset()

    def get_models(self):
        return [self.net]

    def reset(self):
        self.predict_depth = None

    def batch_predict(self, ids, states, done):
        # self.net.eval()
        self.net.train()
        full_hidden_state = self.full_hidden_state

        # -- Put to zero previously finished games
        done_idx = done.nonzero()
        if done_idx.nelement() > 0:
            done_ids = ids.index_select(0, done_idx.squeeze(1))
            for ss in full_hidden_state:
                for s in list(ss):
                    s.data.index_fill_(0, done_ids, 0)
        # -- Select the relevant hidden states
        hidden_state = self.net.slice_hidden(full_hidden_state, ids)

        # -- Predict
        p, _, h, _ = self.net(Variable(states, volatile=True), hidden_state)

        # -- Put new hidden states in full hidden state
        for ss, ss_new in zip(full_hidden_state, h):
            if isinstance(ss, Variable):
                ss = [ss]
                ss_new = [ss_new]
            for s, s_new in zip(list(ss), list(ss_new)):
                s.data.index_fill_(0, ids, 0)
                s.data.index_add_(0, ids, s_new.data)
                s.detach_()

        # Sample actions
        actions = p.multinomial().data.squeeze(1)
        return actions

    def act(self, obs, reward, done, is_training, actions=None, **kwargs):
        if not is_training:
            # self.net.eval()
            return self.predict(obs, reward, done, **kwargs)
        self.net.train()
        not_done = (1 - done)

        have_more_games = not_done.byte().any()

        hidden_state = self.hidden_state

        if self.hidden_state is None:
            assert self.clock == 0
            __BS = obs.size(0)
            hidden_state = self.hidden_state = self.net.init_hidden(__BS)
            self.live_idx = live_idx = torch.linspace(0, __BS - 1, __BS) \
                .type(self.dtype.LongTensor)
            if "next_state_depth" in self.aux_tasks:
                self.predict_depth = torch.zeros(self.state_dim[0]) \
                    .type(self.dtype.FloatTensor)
                predict_depth_idx = (torch.rand(1) * self.state_dim[0]).int()[0]
                self.predict_depth[predict_depth_idx] = 1
                self.predict_depth_idx = torch.LongTensor([predict_depth_idx]) \
                    .type(self.dtype.LongTensor)
                self.predict_depth = self.predict_depth.unsqueeze(0) \
                    .expand(__BS, self.state_dim[0])

        if have_more_games:
            not_done_idx = not_done.nonzero().view(-1)
            non_done_states = obs.index_select(0, not_done_idx)
            hidden_state = self.net.slice_hidden(hidden_state, not_done_idx)
        else:
            not_done_idx = torch.LongTensor(0).type_as(done)
            non_done_states = None

        action = None
        if have_more_games:
            state = non_done_states.float()

            # select Action
            aux_in = {}
            if "next_state_depth" in self.aux_tasks:
                self.predict_depth = self.predict_depth. \
                    index_select(0, not_done_idx)
                aux_in["next_state_depth"] = Variable(self.predict_depth)

            probs, state_value, new_hidden_state, other_task = \
                self.net(Variable(state), hidden_state,
                         aux_input=aux_in, **kwargs)

            if actions is None:
                action = probs.multinomial().data
            else:
                action = actions.unsqueeze(1).index_select(0, not_done_idx)

        if self._o is not None and is_training:
            self.saved_transitions.append(SavedTransition(
                self._o, self._a, self._v, self._other_task, reward.float(),
                obs.float(), not_done_idx, self.live_idx, self._probs))

        if have_more_games:
            self.live_idx = live_idx = \
                self.live_idx.index_select(0, not_done_idx)
            self._o = state
            self._a = action
            self._v = state_value
            self._probs = probs
            self._other_task = other_task
            self.hidden_state = new_hidden_state
            self.clock = self.clock + 1
        else:
            self.optimized_ = False
            self.finish_episode()
            self.clock = 0
            self.hidden_state = None
            self.predict_depth = None

            self._o, self._a, self._v, self._probs = None, None, None, None
            batch_size = self.batch_size
            self.live_idx = torch.linspace(0, batch_size - 1, batch_size) \
                .type(self.dtype.LongTensor)

        self.step_cnt += 1

        # MUST RETURN SAME SIZE AS OBS SIZE
        action_holder = torch.LongTensor(reward.size(0)) \
            .type(self.dtype.LongTensor)
        action_holder.fill_(3)
        if action is not None:
            action_holder.scatter_(0, not_done_idx, action.view(-1).long())
        return action_holder

    def predict(self, obs, reward, done, **kwargs):
        not_done = (1 - done)
        have_more_games = not_done.byte().any()

        hidden_state = self.hidden_state

        if self.hidden_state is None:
            if self.predict_agent_type:
                self.crt_agent_type = self.true_agent_type.clone()
                self.crt_agent_type = self.crt_agent_type.type_as(not_done).float()

            assert self.clock == 0
            __BS = obs.size(0)
            hidden_state = self.hidden_state = self.net.init_hidden(__BS)
            self.live_idx = live_idx = torch.linspace(0, __BS - 1, __BS) \
                .type(self.dtype.LongTensor)
            # if "next_state_depth" in self.aux_tasks:
            #     self.predict_depth = torch.zeros(self.state_dim[0]) \
            #         .type(self.dtype.FloatTensor)
            #     predict_depth_idx = (torch.rand(1) * self.state_dim[0]).int()[0]
            #     self.predict_depth[predict_depth_idx] = 1
            #     self.predict_depth_idx = torch.LongTensor([predict_depth_idx]) \
            #         .type(self.dtype.LongTensor)
            #     self.predict_depth = self.predict_depth.unsqueeze(0) \
            #         .expand(__BS, self.state_dim[0])


        if have_more_games:
            not_done_idx = not_done.nonzero().view(-1)
            non_done_states = obs.index_select(0, not_done_idx)
            if self.predict_agent_type:
                self.crt_agent_type = self.true_agent_type.index_select(0, not_done_idx).type_as(not_done).float()

            hidden_state = self.net.slice_hidden(hidden_state, not_done_idx)
        else:
            self.ep_step = 0
            self.ep_training += 1

            if self.predict_agent_type and self.ep_step < 10:
                self.optimizer_aux.step()
                self.optimizer_aux.zero_grad()

            not_done_idx = torch.LongTensor(0).type_as(done)
            non_done_states = None

        action = None
        if have_more_games:
            state = non_done_states.float()

            # select Action
            probs, state_value, new_hidden_state, other_task = \
                self.net(Variable(state, volatile=True), hidden_state,
                         aux_input={}, **kwargs)


            # print(probs)
            if self.predict_max:
                _, action = probs.max(1)
            else:
                action = probs.multinomial()

            if self.predict_agent_type and self.ep_step < 10:
                # - TRAIN MODE
                prediction = other_task["agent_type"]
                true_agent_type = self.crt_agent_type
                loss_agent_type = nn.MSELoss()(prediction,
                                               Variable(true_agent_type,
                                                        requires_grad=False))
                crt_idx = (self.ep_training, self.step_cnt)
                self.losses["agent_type"][0].update(loss_agent_type.data[0],
                                                 index=crt_idx)
                pred_max = (prediction.data > 0.5).byte()
                # print(true_agent_type)
                # print(pred_max)
                # print(pred_max.nelement())
                acc = ((true_agent_type.byte() == pred_max[:,0]).long()).sum() / pred_max.nelement()
                self.losses["agent_type"][1].update(acc,
                                                 index=crt_idx)

                astar_cnt = true_agent_type.sum()
                random_cnt = true_agent_type.nelement() - astar_cnt
                print("Ep: {} Step: {} _ A*: {:10}\t R: {:10}\t AGENT_TYPE: Loss: {} \t__ Acc: {}\t ".format(self.ep_training, self.ep_step,astar_cnt, random_cnt,
                                                                                                         loss_agent_type.data[0],
                                                                                                         acc))
                # - TRAIN MODE
                if self.aux_tasks["agent_type"][0]:
                    loss_agent_type.backward()

                    # save model :)
                # - TEST MODE
                else:
                    pass

        if have_more_games:
            self.live_idx = live_idx = \
                self.live_idx.index_select(0, not_done_idx)
            self.hidden_state = new_hidden_state
        else:
            self.hidden_state = self.net.init_hidden(self.batch_size)

            batch_size = self.batch_size
            self.live_idx = torch.linspace(0, batch_size - 1, batch_size) \
                .type(self.dtype.LongTensor)

        self.step_cnt += 1
        self.ep_step += 1

        # MUST RETURN SAME SIZE AS OBS SIZE
        action_holder = torch.LongTensor(reward.size(0)) \
            .type(self.dtype.LongTensor)
        action_holder.fill_(3)
        if action is not None:
            action_holder.scatter_(
                0, not_done_idx, action.data.view(-1).long())

        return action_holder

    def forget_current_batch(self):
        self.optimizer.zero_grad()
        del self.saved_transitions[:]
        self.reset()
        self.optimized_ = False
        self.clock = 0
        self.hidden_state = None
        self.predict_depth = None

        self._o, self._a, self._v, self._probs = None, None, None, None
        batch_size = self.batch_size
        self.live_idx = torch.linspace(0, batch_size - 1, batch_size) \
            .type(self.dtype.LongTensor)


    def finish_episode(self):
        new_time = time.time()
        print("-------------> {:f} seconds".format(new_time - self.last_time))
        self.last_time = new_time

        saved_transitions = self.saved_transitions
        value_loss = 0
        rewards = []
        rewards_norm = []
        rewards_true_norm = []

        for i in range(len(saved_transitions))[::-1]:
            r = saved_transitions[i].reward.clone()
            playing_idx = saved_transitions[i].playing_idx
            if i < len(saved_transitions) - 1:
                r.index_add_(0, playing_idx, rewards[0] * self.gamma)

            rewards.insert(0, r)

        all_r = torch.cat(rewards, 0)
        mean_r = all_r.mean()
        std_r = all_r.std() + np.finfo(np.float32).eps
        rewards_mean_step = []
        backprop_reward = []
        for i in range(len(rewards)):
            mean_r = rewards[i].mean()

            rewards_norm.append((rewards[i] - mean_r) / std_r)
            rewards_true_norm.append(rewards[i] / ENV_CAUGHT_REWARD)
            rewards_mean_step.append(rewards_true_norm[i].mean())

            # Normalize action advantage in 1.0
            if self.aux_tweaks["reinforce_a3c"][0]:
                # Original a3c
                reward_adv = (rewards_true_norm[i] - \
                             saved_transitions[i].value.data[:, 0])
            else:
                reward_adv = rewards_true_norm[i] - rewards_mean_step[i]

            backprop_reward.append(reward_adv)

        # -------- Solve other tasks; Predict next state --------------------
        loss_aux = {}
        if "next_state_depth" in self.aux_tasks:
            loss_ = 0
            for i in range(len(saved_transitions)):
                pred_depth = saved_transitions[i].other_task["next_state_depth"]
                next_state_D = saved_transitions[i].next_obs \
                    .index_select(1, self.predict_depth_idx)

                next_state_D = Variable(next_state_D.view(-1, 81))
                loss_ += nn.BCELoss()(pred_depth, next_state_D)
            loss_aux["next_state_depth"] = loss_

        if "next_reward" in self.aux_tasks:
            loss_ = 0
            for i in range(len(saved_transitions)):
                pred_next_r = saved_transitions[i].other_task["next_reward"]
                loss_ += nn.MSELoss()(pred_next_r,
                                      Variable(saved_transitions[i].reward /
                                               ENV_CAUGHT_REWARD))
            loss_aux["next_reward"] = loss_

        use_entropy = False
        if "entropy" in self.aux_tasks:
            loss_aux["entropy"] = 0
            use_entropy = True


        # ----------------------------------------------------------------------

        # TODO Move hardcoded decaying clamp to config
        # # -- CHANGE CLAMP::::
        # clamp_coef = next(self.clamp_decay)
        # if (self.ep_training + 1) % 250 == 0:
        #     print(clr("CHANGING CLAMP !!! _______ {} ________---- *&^@*&".format(clamp_coef), "green"))
        #
        # self.min_prob_clamp = clamp_coef
        # self.max_prob_clamp = 1.-clamp_coef

        # if (self.ep_training + 1) % 250 == 0:
        #     clamp_val = [0.3, 0.2, 0.1, 0.01, 0.001, 0.0001, 0.00001]
        #     pos = (self.ep_training + 1) // 250
        #     print(clr("CHANGING CLAMP !!! _______ {} ________---- *&^@*&".format(clamp_val[pos]), "red"))
        #     self.min_prob_clamp = clamp_val[pos]
        #     self.max_prob_clamp = 1. - clamp_val[pos]

        actor_loss = None
        for i in range(len(saved_transitions)):
            tr = saved_transitions[i]
            probs = tr.probs

            # Record entropy
            H = -(torch.log(probs + 1e-18) * probs).sum(1).mean()
            H = H / math.log(2)
            crt_idx = (self.ep_training, self.step_training)
            self.entropy.update(H.data[0], index=crt_idx)

            reward = backprop_reward[i]
            grad = probs.data.new().resize_as_(probs.data).fill_(0)

            grad.scatter_(1, tr.action, reward.unsqueeze(1))

            # - Running on the left
            clamp_value = next(self.clamp_decay)
            self.min_prob_clamp = clamp_value
            self.max_prob_clamp = 1 - clamp_value
            grad /= -torch.clamp(probs.data, self.min_prob_clamp,
                                 self.max_prob_clamp)

            l = torch.dot(Variable(grad), probs)
            actor_loss = l if actor_loss is None else (actor_loss + l)

            # Aux Task entropy
            if use_entropy:
                loss_aux["entropy"] += -H

            # TODO put if
            # tr.action.reinforce(reward.unsqueeze(1))
            value_loss += nn.MSELoss()(tr.value[:, 0],
                                       Variable(rewards_true_norm[i]))
            self.step_training += 1

        self.optimizer.zero_grad()

        loss_save = self.losses
        aux_tasks = self.aux_tasks
        ep = self.ep_training
        loss_save["critic"].update(value_loss.data[0], index=ep)
        loss_save["actor"].update(actor_loss.data[0], index=ep)
        full_loss = actor_loss

        if self.use_critic:
            full_loss += value_loss

        for k, v in loss_aux.items():
            full_loss += v * aux_tasks[k][0]
            loss_save[k].update(v.data[0], index=ep)

        full_loss.backward()

        loss_save["total"].update(full_loss.data[0], index=ep)

        """
        loss_msg = "Losses:: "
        for k, v in loss_save.items():
            loss_msg += "{}_loss: {:.5f}. ".format(k, v.val)

        print(loss_msg)
        """

        # `clip_grad_norm` helps prevent the exploding gradient problem
        torch.nn.utils.clip_grad_norm(self.net.parameters(), self.clip_grad)

        self.optimizer.step()

        # JUST TRY CHANGE OPTIMIZER LEARNING RATE
        # TODO Put Parameter in config
        if self.adjust_lr:
            if (self.ep_training+1) % self.adjust_lr_freq == 0:
                lr = self.lr * (self.adjust_lr_coef**((self.ep_training+1)
                                                      //self.adjust_lr_freq))

                print(clr("NEW LEARNING RATE::: {}".format(lr), "red"))
                for param_group in self.optimizer.param_groups:
                    param_group['lr'] = lr

        self.ep_training += 1
        del self.saved_transitions[:]
        self.reset()


