# Village People, 2017

# This script trains an agent on Malmo.

import torch
import time
from termcolor import colored as clr
from models import get_model
from utils import read_config
from utils import AtomicStatistics
import numpy as np
import random

# from utils import AverageMeter
from worker_scripts.synchronised_dockers_malmo_training import \
    train_from_dockers


def print_info(message):
    print(clr("[MAIN] ", "yellow") + message)


def startup_message(cfg):
    print_info("Training {:s} versus {:s}!".format(
        clr("{:s}<{:s}>".format(cfg.agent.type, cfg.model.name),
            "white", "on_magenta"),
        clr("others", "white", "on_cyan")
    ))


def main():
    print_info("Booting...")
    cfg = read_config()
    startup_message(cfg)

    # -- Print important conf info
    print_info("\nCONFIG >")
    print_info(clr("P_FOCUS_minecraft: {}".format(cfg.envs.minecraft.p_focus),
                   "red"))

    # -- Configure Torch
    if cfg.general.seed > 0:
        torch.manual_seed(cfg.general.seed)
        np.random.seed(cfg.general.seed)
        random.seed(cfg.general.seed)
        torch.cuda.manual_seed_all(cfg.general.seed)

    print_info("Torch setup finished.")

    # -- Configure model
    model = get_model(cfg.model.name)(cfg.model)
    if cfg.general.use_cuda:
        model.cuda()

    if isinstance(cfg.model.load, str):
        checkpoint = torch.load(cfg.model.load)
        if "max_r_ep" in checkpoint:
            reward = checkpoint['max_r_ep']
        else:
            reward = checkpoint['reward']
        print("Loading Model: {} Mean reward/ episode: {}"
              "".format(cfg.model.load, reward))
        model.load_state_dict(checkpoint['state_dict'])

    print_info("Model {:s} initalized.".format(clr(cfg.model.name, "red")))

    # -- Shared objects
    objects = {
        "model": model,
        "stats_leRMS": AtomicStatistics()
    }

    # -- Start training

    print_info("Training starts now!")
    start_time = time.time()
    train_from_dockers(objects, cfg)
    total_time = time.time() - start_time
    print_info("Training and no evluation done in {:.2f}!".format(total_time))


if __name__ == "__main__":
    main()
