# Village People, 2017

# This is the model we got our best results with.
#
# The model receives the current state, and predicts the policy, the value
# of the state and various other outputs for the auxiliary task.
#
# The model uses BatchNormalization in the first stages of training

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from models.utils import conv_out_dim


ENV_CAUGHT_REWARD = 25

class NextRewardPredictor(nn.Module):
    """This model is used for the prediction of the next reward."""
    def __init__(self, in_size):
        super(NextRewardPredictor, self).__init__()
        self.predictor = nn.Linear(in_size, 1)

    def forward(self, x):
        x = nn.Tanh()(self.predictor(x))
        return x


class NextStateDepthPrediction(nn.Module):
    """This model """
    def __init__(self, in_size, out_size):
        super(NextStateDepthPrediction, self).__init__()

        self.act = nn.ReLU()

        inter = int(in_size / 2)
        self.predictor1 = nn.Linear(in_size, inter)
        self.bn1 = nn.BatchNorm1d(inter)
        self.predictor2 = nn.Linear(inter, out_size)

    def forward(self, x):
        act = self.act
        x = act(self.bn1(self.predictor1(x)))
        x = nn.Sigmoid()(self.predictor2(x))
        return x


class PredictNextState(nn.Module):
    def __init__(self, in_size):
        super(PredictNextState, self).__init__()

        self.act = nn.ReLU(True)
        self.in_size = in_size
        ngf = 64
        nc = 15
        self.first_cond_w = first_cond_w = 3
        first_conv_depth = int(in_size // (first_cond_w ** 2))
        cn_size = first_conv_depth * first_cond_w * first_cond_w
        self.lin1 = nn.Linear(in_size, cn_size)
        self.bnLin = nn.BatchNorm1d(cn_size)

        self.cn2 = nn.ConvTranspose2d(first_conv_depth, 64, kernel_size=3,
                                      stride=1, bias=False)
        self.bn2 = nn.BatchNorm2d(64)
        self.cn3 = nn.ConvTranspose2d(64, 32, kernel_size=3, stride=1,
                                      bias=False)
        self.bn3 = nn.BatchNorm2d(32)
        self.cn4 = nn.ConvTranspose2d(32, nc, kernel_size=3, stride=1,
                                      bias=False)
        self.bn4 = nn.BatchNorm2d(nc)

    def forward(self, x):
        act = self.act
        x = act(self.bnLin(self.lin1(x)))
        x = x.view(x.size(0), -1, self.first_cond_w, self.first_cond_w)
        x = act(self.bn2(self.cn2(x)))
        x = act(self.bn3(self.cn3(x)))
        x = self.cn4(x)
        x = nn.Sigmoid()(x)

        return x.view(-1, 15, 9, 9)


def sampler(input_, tau=100):
    noise = Variable(torch.randn(input_.size(0), input_.size(1))
                     .type_as(input_.data)) / tau

    x = input_ + noise
    return x

def sample_gumbel(input):
    noise = torch.rand(input.size()).type_as(input.data)
    eps = 1e-20
    noise.add_(eps).log_().neg_()
    noise.add_(eps).log_().neg_()
    return Variable(noise)

def gumbel_softmax_sample(input, temperature=1.0):
    noise = sample_gumbel(input)
    x = (input + noise) / temperature
    x = F.softmax(x)
    return x.view_as(input)

class AgentTypeIntuition(nn.Module):
    """This model """
    def __init__(self, in_size, layers_size, out_size):
        super(AgentTypeIntuition, self).__init__()

        self.act = nn.ReLU()

        layers_size = layers_size*2
        self.predictor1 = nn.Linear(in_size, layers_size)
        self.bn1 = nn.BatchNorm1d(layers_size)
        self.predictor2 = nn.Linear(layers_size, layers_size)
        self.bn2 = nn.BatchNorm1d(layers_size)
        self.predictor3 = nn.Linear(in_size, out_size)

    def forward(self, x):
        act = self.act
        x = act(self.bn1(self.predictor1(x)))
        x = act(self.bn2(self.predictor2(x)))
        x = nn.Sigmoid()(self.predictor3(x))
        return x

def bn_hack(bn, x):
    assert isinstance(x, Variable)
    y = (x - Variable(
        bn.running_mean.unsqueeze(0).expand_as(x.data))) / \
        Variable(torch.sqrt(
            bn.running_var.unsqueeze(0).expand_as(x.data) + .00001)) * \
        Variable(bn.weight.data.unsqueeze(0).expand_as(x.data)) + \
        Variable(bn.bias.data.unsqueeze(0).expand_as(x.data))
    return y

class Policy(nn.Module):
    """ PigChase Model for the 18BinaryView batch x 18 x 9 x 9.

    Args:
        state_dim (tuple): input dims: (channels, width, history length)
        action_no (int): no of actions
        hidden_size (int): size of the hidden linear layer
    """

    def __init__(self, config):
        super(Policy, self).__init__()

        state_dim = (18, 9, 1)
        action_no = 3
        hidden_size = 256
        dropout = 0.1

        self.rnn_type = rnn_type = "GRUCell"
        self.rnn_layers = rnn_layers = 2
        self.rnn_nhid = rnn_nhid = hidden_size

        self.activation = nn.ReLU()

        self.drop = nn.Dropout(dropout)
        self.drop2d = nn.Dropout2d(p=dropout)

        self.in_channels, self.in_width, self.hist_len = state_dim
        self.action_no = action_no
        self.hidden_size = hidden_size
        in_depth = self.hist_len * self.in_channels

        self.conv0 = nn.Conv2d(in_depth, 64, kernel_size=1, stride=1)
        self.bn0 = nn.BatchNorm2d(64)
        self.conv1 = nn.Conv2d(64, 64, kernel_size=3, stride=1)
        self.bn1 = nn.BatchNorm2d(64)
        self.conv2 = nn.Conv2d(64, 64, kernel_size=3, stride=1)
        self.bn2 = nn.BatchNorm2d(64)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=1)
        self.bn3 = nn.BatchNorm2d(64)

        map_width1 = conv_out_dim(self.in_width, self.conv1)
        map_width2 = conv_out_dim(map_width1, self.conv2)
        map_width3 = conv_out_dim(map_width2, self.conv3)
        # map_width4 = conv_out_dim(map_width3, self.conv4)

        lin_size = 64 * map_width3 ** 2

        # self.lin1 = nn.Linear(lin_size, lin_size)
        # self.bnLin1 = nn.BatchNorm1d(lin_size)

        self.rnn1 = getattr(nn, rnn_type)(lin_size, rnn_nhid)
        self.rnn2 = getattr(nn, rnn_type)(rnn_nhid, rnn_nhid)

        self.bnRnn1 = nn.BatchNorm1d(rnn_nhid)
        self.bnRnn2 = nn.BatchNorm1d(rnn_nhid * self.rnn_layers)

        self.lin2 = nn.Linear(rnn_nhid * self.rnn_layers, hidden_size)

        lin_size_3 = hidden_size
        self.lin3 = nn.Linear(hidden_size, lin_size_3)

        self.bnLin2 = nn.BatchNorm1d(hidden_size)
        self.bnLin3 = nn.BatchNorm1d(lin_size_3)

        self.action_head = nn.Linear(lin_size_3, action_no)
        self.value_head = nn.Linear(lin_size_3, 1)
        self.bn_value_head = nn.BatchNorm1d(1)

        # ---- Aux tasks

        self.aux_predictors = []
        aux_tweaks = {t[0]:t[1:] for t in config.auxiliary_tweaks}

        print(aux_tweaks)

        if "noise" in aux_tweaks:
            self.action_noise = aux_tweaks["noise"][0]
        else:
            self.action_noise = 0

        if "no_final_bn" in aux_tweaks:
            self.use_final_bn = False
        else:
            self.use_final_bn = True

        if "gumbel" in aux_tweaks:
            self.use_gumbel = aux_tweaks["gumbel"][0]
            self.g_temperature = float(aux_tweaks["gumbel_temperature"][0])
        else:
            self.use_gumbel = False

        aux_tasks = {t[0]:t[1:] for t in config.auxiliary_tasks}
        if "next_reward" in aux_tasks:
            _input_size = rnn_nhid * self.rnn_layers
            self.next_reward = NextRewardPredictor(_input_size)
            self.aux_predictors.append(("next_reward", self.next_reward))

        if "next_state_depth" in aux_tasks:
            _input_size = rnn_nhid * self.rnn_layers + state_dim[0]
            self.next_state_depth = NextStateDepthPrediction(_input_size,
                                                             state_dim[1] ** 2)
            self.aux_predictors.append(("next_state_depth",
                                        self.next_state_depth))

        if "agent_type" in aux_tasks:
            _input_size = rnn_nhid * self.rnn_layers
            self.agent_type = AgentTypeIntuition(_input_size, hidden_size, 1)
            self.aux_predictors.append(("agent_type",
                                        self.agent_type))

    def forward(self, x, hidden_states, bn=True, aux_input={}):
        act = self.activation

        batch_size = x.size(0)

        # if bn:

        x = act(self.bn0(self.conv0(x)))
        x = act(self.bn1(self.conv1(x)))
        x = act(self.bn2(self.conv2(x)))
        x = act(self.bn3(self.conv3(x)))

        out_conv = x.view(x.size(0), -1)

        hidden0 = self.rnn1(out_conv, hidden_states[0])
        if self.rnn_type == 'LSTMCell':
            x = hidden0[0]
        else:
            x = hidden0

        hx = [x]

        hidden1 = self.rnn2(x, hidden_states[1])
        if self.rnn_type == 'LSTMCell':
            x = hidden1[0]
        else:
            x = hidden1

        all_hidden = [hidden0, hidden1]
        hx.append(x)
        x = torch.cat(hx, 1)

        aux_predictions = {}
        if self.training:
            for (task, predictor) in self.aux_predictors:
                if task == "next_reward":
                    aux_predictions[task] = predictor(x)
                if task == "agent_type":
                    aux_predictions[task] = predictor(Variable(x.data,
                                                               requires_grad=
                                                               False))
                if task == "next_state_depth" and "next_state_depth" in aux_input:
                    in_next = torch.cat([aux_input["next_state_depth"], x], 1)
                    aux_predictions[task] = predictor(in_next)

        if self.use_final_bn:
            if batch_size < 24:
                # TODO
                x = self.lin2(x)
                x = bn_hack(self.bnLin2, x)
                x = act(x)
                x = self.lin3(x)
                x = bn_hack(self.bnLin3, x)
                x = act(x)
            else:
                x = self.bnLin2(self.lin2(x))
                x = act(x)
                x = act(self.bnLin3(self.lin3(x)))
        else:
            x = act((self.lin2(x)))
            x = act((self.lin3(x)))

        action_scores = self.action_head(x)
        state_values = self.value_head(x)

        # # Original sampler
        if self.use_gumbel:
            _t = self.g_temperature if self.training else .0
            action_prob = gumbel_softmax_sample(action_scores, temperature=_t)
        elif self.action_noise > 0:
            # action_prob = sampler(F.softmax(action_scores),
            #                       tau=self.action_noise)
            action_prob = F.softmax(sampler(action_scores,
                                            tau=self.action_noise))
        else:
            action_prob = F.softmax(action_scores)

        # action_prob = F.softmax(sampler(action_scores, tau=self.action_noise))
        return action_prob, state_values, all_hidden, aux_predictions

    def init_hidden(self, bsz):
        hidden_states = []
        weight = next(self.parameters()).data
        # Hidden 0
        if self.rnn_type == 'LSTMCell':
            hidden_states.append((Variable(weight.new(bsz, self.rnn_nhid)
                                           .zero_()),
                                  Variable(weight.new(bsz, self.rnn_nhid)
                                           .zero_())))
        else:
            hidden_states.append(Variable(weight.new(bsz, self.rnn_nhid)
                                          .zero_()))
        # Hidden 0
        if self.rnn_type == 'LSTMCell':
            hidden_states.append((Variable(weight.new(bsz, self.rnn_nhid)
                                           .zero_()),
                                  Variable(weight.new(bsz, self.rnn_nhid)
                                           .zero_())))
        else:
            hidden_states.append(Variable(weight.new(bsz, self.rnn_nhid)
                                          .zero_()))

        return hidden_states

    def slice_hidden(self, hidden_state, not_done_idx):
        hidden_states = []

        i = 0
        if self.rnn_type == 'LSTMCell':
            hidden_states.append((hidden_state[i][0]
                                  .index_select(0, Variable(not_done_idx)),
                                  hidden_state[i][1].index_select(0, Variable(
                                      not_done_idx))))
        else:
            hidden_states.append(hidden_state[i]
                                 .index_select(0, Variable(not_done_idx)))
        i = 1
        if self.rnn_type == 'LSTMCell':
            hidden_states.append((hidden_state[i][0]
                                  .index_select(0, Variable(not_done_idx)),
                                  hidden_state[i][1].index_select(0, Variable(
                                      not_done_idx))))
        else:
            hidden_states.append(hidden_state[i]
                                 .index_select(0, Variable(not_done_idx)))
        return hidden_states

    def get_attributes(self):
        return (self.input_channels, self.hist_len, self.action_no,
                self.hidden_size)
