"""
Kaixhin/malmo-challenge
https://github.com/Kaixhin/malmo-challenge
https://github.com/Kaixhin/malmo-challenge/blob/master/ai_challenge/pig_chase/pc_environment.py

"""

import socket
import time
import torch
from torch import multiprocessing as mp
from pig_chase.common import ENV_AGENT_NAMES
from pig_chase.agent import PigChaseChallengeAgent, RandomAgent
from pig_chase.environment import PigChaseEnvironment, \
    PigChaseSymbolicStateBuilder
from termcolor import colored as clr

# Taken from Minecraft/launch_minecraft_in_background.py, but adapted :)

# Taken from Minecraft/launch_minecraft_in_background.py
def _port_has_listener(client):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(client)
        sock.settimeout(1)
        sock.recv(1024)
        sock.close()
        return False
    except socket.timeout:
        return True
    except Exception as e:
        return False


def search_malmo(_id, player):
    ip = 2
    while True:
        client = ("172.17.0.{:d}".format(ip), 10000)
        print("[{:s}-{:s}] Trying on ".format(str(_id), str(player)), client)
        if _port_has_listener(client):
            print("[{:s}-{:s}] Connected on ".format(str(_id), str(player)),
                  client, "!!!")
            return client
        ip = ip + 1 if ip < 20 else 2

class EnvNoLaunch(object):

    def __init__(self, clients, _id, agent_builder):

        self._id = _id

        if clients is None:
            clients = [search_malmo(_id, "A"), search_malmo(_id, "B")]

        else:
            while not _port_has_listener(clients[0]):
                print("[" + str(_id) + "] Nu raspunde " + str(clients[0]))
                time.sleep(4)

            print("[" + str(_id) + "] Am gasit un malmo la ", clients[0])

            while not _port_has_listener(clients[1]):
                print("[" + str(_id) + "] Nu raspunde " + str(clients[1]))
                time.sleep(4)

            print("[" + str(_id) + "] Am gasit un malmo la ", str(clients[1]))

            print(clr("[" + str(_id) + "] Au inceput sa joace " +
                      str(clients[0]) +
                      " cu " + str(clients[1]), "white", "on_magenta"))

        # ______________________________________________________________________

        # Set up partner agent env in separate process _________________________
        self.p = p = mp.Process(target=self._run_partner, args=(clients,))
        p.daemon = True
        p.start()
        time.sleep(4)

        # Set up agent env _____________________________________________________
        self.env = PigChaseEnvironment(clients, agent_builder, role=1,
                                       randomize_positions=True)

    # Transform in torch tensor
    def _map_to_observation(self, observation):
        state_ = torch.LongTensor(observation).unsqueeze(0)
        return state_

    def _map_env_return(self, observation, reward, done):
        state_ = torch.LongTensor(observation).unsqueeze(0)
        reward_ = torch.LongTensor([int(reward)])
        done_ = torch.LongTensor([int(done)])
        return state_, reward_, done_

    def get_class_label(self):
        return self.agent_type.value() - 1

    def reset(self):
        _id = self._id
        print("[" + str(_id) + "] Dom reset")
        observation = self.env.reset()
        print("[" + str(_id) + "] am reset")
        while observation is None:
            print("[" + str(_id) + "] am primit o pulaaaa")
            # May happen if episode ended with first action of other agent
            observation = self.env.reset()

        return self._map_to_observation(observation)

    def step(self, action):
        observation, reward, done = self.env.do(action)
        return self._map_env_return(observation, reward, done) +  \
               (None,) # Do not return any extra info

    def close(self):
        self.p.terminate()
        return  # TODO: Kill processes + Docker containers


    # Runs partner in separate env
    def _run_partner(self, clients):
        env = PigChaseEnvironment(clients, PigChaseSymbolicStateBuilder(),
                                  role=0, randomize_positions=True)
        agent = PigChaseChallengeAgent(ENV_AGENT_NAMES[0])
        self.agent_type = type(
            agent.current_agent) == RandomAgent and \
                          PigChaseEnvironment.AGENT_TYPE_1 or \
                          PigChaseEnvironment.AGENT_TYPE_2
        print("sunt scalbul si am dat areset")
        print(self.agent_type)
        obs = env.reset(self.agent_type)
        print("sunt scalbul siam terminat areset")
        # print("MALMO AGENT A* ________________\n\n", obs,
        #       "-------------------------------------------------------------")

        reward = 0
        agent_done = False
        while True:
            # Select an action
            if env.done:
                self.agent_type = type(agent.current_agent) == RandomAgent \
                                  and PigChaseEnvironment.AGENT_TYPE_1 \
                                  or PigChaseEnvironment.AGENT_TYPE_2

                obs = env.reset(self.agent_type)
                while obs is None:
                    # this can happen if the episode ended with the first
                    # action of the other agent
                    print('Warning: Challenger received obs == None. _id: {} _ '
                          .format(self._id))
                    obs = env.reset(self.agent_type)

            action = agent.act(obs, reward, agent_done, is_training=True)

            # Take a step
            obs, reward, agent_done = env.do(action)
