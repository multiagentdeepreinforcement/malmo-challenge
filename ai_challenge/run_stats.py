import pandas as pd
import numpy as np
import glob
import re
import os
import seaborn as sns
import torch
import numpy as np

pd.set_option('display.height', 1000)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# EP_DATA = ["actor_loss", "critic_loss", "next_reward_loss",
#            "next_state_depth_loss", "total_loss", "mean_r_step"]
EP_DATA = ["mean_r_step"]
# EP_STEP_DATA = ["episode_time", "games_alive", "reward_sum_step"]
EP_STEP_DATA = ["games_alive", "reward_sum_step"]
EP_STEP_DATA_op = ["max", "sum"]

def apply_conv_smooth(df, conv_by=50):
    for i in df.index:
        df.loc[i] = df[i:i + conv_by].mean()

def get_data_filtered(prefix):
    folders = glob.glob(prefix + "*/")
    folders = list(filter(lambda x: re.match(prefix+"_\d",x)
                                    or x == prefix+'/', folders))
    return folders

def longestSubstringFinder(string1, string2):
    answer = ""
    len1, len2 = len(string1), len(string2)
    for i in range(len1):
        match = ""
        for j in range(len2):
            if (i + j < len1 and string1[i + j] == string2[j]):
                match += string2[j]
            else:
                if (len(match) > len(answer)): answer = match
                match = ""
    return answer

def get_dataframe_gather(folders, filename, basename=None, group_by_ep=False,
                         group_op=None):
    df = []

    for ix, fld in enumerate(folders):
        filepath = os.path.join(fld, filename)
        print(filepath)
        if not os.path.isfile(filepath):
            print("{} not found!!!".format(filepath))
            continue

        new_df = pd.read_csv(filepath)
        rename_column = {}

        if basename is not None:
            folder_name = basename[ix]
        else:
            path, folder_name = os.path.split(os.path.dirname(fld))

        for cl in new_df.columns:
            if cl != "Index":
                rename_column[cl] = folder_name
        new_df = new_df.rename(columns=rename_column)

        if group_by_ep:
            new_df.loc[:, "Index"] = new_df[
                "Index"].apply(eval)
            new_df["Episode"] = new_df["Index"] \
                .apply(lambda x: x[0])
            new_df["Step"] = new_df["Index"] \
                .apply(lambda x: x[1])
            if group_op == "sum":
                new_df = new_df[col_without(new_df, ["Index", "Step"])].groupby(
                    "Episode").sum()
            elif group_op == "max":
                new_df = new_df[col_without(new_df, ["Index", "Step"])].groupby(
                    "Episode").max()

            new_df["Index"] = new_df.index

        df.append(new_df)
        # if df is None:
        #     df = new_df
        # else:
        #     df.merge(new_df, on="Index", how="outer")

    df_merge = pd.concat([x.set_index('Index') for x in df], axis=1)


    return df_merge

def col_without(df, except_):
    return [x for x in df.columns if x not in except_]

def col_contain(df, str_):
    return [x for x in df.columns if str_ in x]

def list_first_nan_ep(df):
    for cl in df.columns:
        first_valid = df[cl].first_valid_index()
        last_valid = df[cl].last_valid_index()
        print("{}:: {} -> {}".format(cl,
                                     df.loc[first_valid, "Episode"],
                                     df.loc[last_valid, "Episode"]))

def get_group(names):
    groups = {}
    for name in names:
        name = re.sub("^/|/$", "", name)
        m = re.findall(".+?(?=_\d)", name)
        if len(m) > 0:
            prefix = "".join(m)
            if prefix in groups:
                groups[prefix].append(name)
            else:
                groups[prefix] = [name]
        else:
            if name in groups:
                groups[name].append(name)
            else:
                groups[name] = [name]
    return groups

OK_AC = glob.glob("experiments/OK_AC/" + "*/")
OK_VP = glob.glob("experiments/OK_VP/" + "*/")
OK_BN = glob.glob("experiments/OK_BN/" + "*/")

# folders = get_data('experiments/TEST_')
# - Get all data from folder
folders = glob.glob("experiments/EXP_STUFF/" + "*/")

print(folders)

data_ep = dict()
for filename in EP_DATA:
    data_ep[filename] = get_dataframe_gather(folders, filename)
    data_ep[filename] = data_ep[filename].rename(columns={"Index":"Episode"})

data_step = dict()
group_from_start = True
for ix, filename in enumerate(EP_STEP_DATA):
    data_step[filename] = get_dataframe_gather(folders, filename, group_by_ep=group_from_start, group_op=EP_STEP_DATA_op[ix])
    # Change from tuple to id
    # data_step[filename].loc[:, "Index"] = data_step[filename]["Index"].apply(eval)
    # data_step[filename]["Episode"] = data_step[filename]["Index"]\
    #     .apply(lambda x: x[0])
    # data_step[filename]["Step"] = data_step[filename]["Index"]\
    #     .apply(lambda x: x[1])
    print("Done {:s}".format(filename))

# GET GROUPS
groups = get_group(col_without(data_ep["mean_r_step"],["Index", "Step", "Episode"]))

# ------------------------------------------------------------------------------
# Change columns name in dataset

def change_name(x):
    # A3C - experiment
    # get_name = lambda base_name: "Type:{} Entropy:{} Clamp:{}".format(
    #     re.findall("auxiliary_tweaks_(.*?(?=_))", base_name)[0],
    #     re.findall(r'auxiliary_tasks_(.*?(?=_))', base_name)[0],
    #     re.findall(r'auxiliary_tweaks_.*_(.*)_[_\d_]?', base_name)[0])
    # sub_list = {"vp": "STD",
    #             "a3c": "AC",
    #             ":clamp":":yes",
    #             ":Clamp":":yes",
    #             "noClamp":"no",
    #             "noEntropy":"no",
    #             "entropy":"yes"}

    # # BN comparison
    # get_name = lambda base_name: "Batch:{} Batch_norm:{}".format(
    #     re.findall("batch_size_(\d*)", base_name)[0],
    #     re.findall(r'auxiliary_tweaks_(.*?(?=_))', base_name)[0])
    # sub_list = {"std": "yes",
    #             "no_bn":"no"}
    # # FINE TUNE
    # get_name = lambda base_name: "Type:{} P_focus:{}".format(
    #     re.findall("B-[\d]*_(.*)__1024", base_name)[0],
    #     re.findall("p_focus-(\d*)_", base_name)[0],
    #     re.findall(r'_B-(\d*)_', base_name)[0])
    # sub_list = {"std": "STD",
    #             "no_critic":"No_BN",
    #             "P_focus:5":"P_focus:50",
    #             "P_focus:1":"P_focus:100"}

    # SIMPLE format: <type>_seed_<x>_
    get_name = lambda base_name: "Type:{} Seed:{}".format(
        re.findall("exp_(.*)_seed", base_name)[0],
        re.findall("seed_(\d*)_", base_name)[0])
    sub_list = {"std": "STD",
                "no_critic":"No_BN",
                "P_focus:5":"P_focus:50",
                "P_focus:1":"P_focus:100"}

    new_name = get_name(x)
    #
    for x, y in sub_list.items():
        if x in new_name:
            new_name = re.sub(x, y, new_name)
    return new_name

def change_df_col(df):
    ren = {}
    for x in df.columns:
        print(x)
        ren[x] = change_name(x)
    return df.rename(columns=ren)

# ------------------------------------------------------------------------------
# BN comparison ---------------------------------------------

#Arange at same ep
df = rew_ep_per_game.copy()
df_128 = df[col_contain(df, "batch_size_128")]
df_128 = df_128[:len(df_128)-2]
df_128 = df_128.copy()
apply_conv_smooth(df_128) # ---- Apply smoothing (inplace)

df_1024 = df[col_contain(df, "batch_size_1024")]
last_ep = 0
for x in df_1024.columns:
    last_ep = max(last_ep, df_1024[x].last_valid_index())
df_1024 = df_1024[:last_ep]
df_1024 = df_1024.copy()
df_1024 = df_1024[:len(df_1024)-2]
apply_conv_smooth(df_1024) # ---- Apply smoothing (inplace)

df_1024["index"] = df_1024.index * 8
# df_1024 = df_1024.set_index("index")



df = pd.concat([df_128, df_1024.set_index("index")], axis=1)
# ------------------------------------------------------------------------------

step_r = data_ep["mean_r_step"].columns
reward = data_step["reward_sum_step"]
live = data_step["games_alive"]
# Check number of max unique live games per exp
if group_from_start:
    live.max().nunique()
else:
    live[col_without(live,["Index", "Step"])].groupby("Episode").max().nunique()

# get max games per experiment
max_games_exp = live[col_without(live,["Index", "Step", "Episode"])].max()
if group_from_start:
    rew_ep = reward[col_without(reward, ["Index", "Step"])]
else:
    rew_ep = reward[col_without(reward,["Index", "Step"])].groupby("Episode").sum()

# Calculate Reward per ep per game
rew_ep_per_game = rew_ep.copy()
col_sorted = list(rew_ep_per_game.columns)
col_sorted.sort()

for cl in rew_ep_per_game.columns:
    rew_ep_per_game.loc[:, cl] = rew_ep_per_game[cl] / max_games_exp[cl]
rew_ep_per_game[col_sorted].max(0)

# step_r[col_without(step_r,["Episode"])].plot()

rew_ep_per_game.plot()
# - mean by group
rew_ep_per_game.loc[:, "Ep_group_0"] = rew_ep_per_game.index//50*50
rew_ep_per_game.groupby("Ep_group_0").max().plot()

#cl = rew_ep_per_game.columns
# cl = list(filter(lambda x: not x.startswith("Ep_"), cl))
# rew_ep_per_game
# rew_ep_per_game.plot(y=cl, x="Ep_group_0")

# - Modify for seaborn
#--- WTF]
import seaborn.timeseries

def _plot_std_bars(*args, central_data=None, ci=None, data=None, **kwargs):
    std = data.std(axis=0)
    ci = np.asarray((central_data - std, central_data + std))
    kwargs.update({"central_data": central_data, "ci": ci, "data": data})
    seaborn.timeseries._plot_ci_bars(*args, **kwargs)

def _plot_std_band(*args, central_data=None, ci=None, data=None, **kwargs):
    std = data.std(axis=0)
    ci = np.asarray((central_data - std, central_data + std))
    kwargs.update({"central_data": central_data, "ci": ci, "data": data})
    seaborn.timeseries._plot_ci_band(*args, **kwargs)

seaborn.timeseries._plot_std_bars = _plot_std_bars
seaborn.timeseries._plot_std_band = _plot_std_band
#---------------------------------------------------

df = rew_ep_per_game.copy()
# df = df2.loc[:9999].copy()
df = df.loc[:9999]
df = df.loc[:995]
df = df.copy()

# SMOOTHING CONVOLUTION:
conv_by = 50
for i in df.index:
    df.loc[i] = df[i:i+conv_by].mean()


values = []
unit = []
time_stamp = []
condition = []
group_values = 1
df["timestamp"] = (df.index//group_values)*group_values
id_ = df.index - df["timestamp"]
id_ = np.apply_along_axis(lambda y: [str(i) for i in y], 0, id_.values)

for group_name in list(groups.keys())[:]:
    for cl_ in groups[group_name]:
        values += df[cl_].values.tolist()
        condition += [group_name] * len(df[cl_].values)
        unit += list(map(lambda x: cl_ + "_" + x, id_))
        time_stamp += df["timestamp"].tolist()


sea_df = pd.DataFrame({"Values":values,
                       "Unit":unit,
                       "Condition": condition,
                       "Time":time_stamp})
# sea_df.loc[:, "Condition"] = sea_df.Condition.apply(change_name)

sea_df = sea_df.sort_values("Condition")
sea_df = sea_df[~sea_df["Values"].isnull()]
# sea_df["Experiment"] = "Orig_"

# # - With variation
# sns.tsplot(data=sea_df, time="Episode", unit="Seed",
#            condition="Experiment", value="Values")


# TRANSFORM TO SEADATA STUFFF:::
paper_rc = {'lines.linewidth': 1, 'lines.markersize': 10}
sns.set_context("paper", rc = paper_rc)

# -Plot each individual trace
import matplotlib.pyplot as plt
f, ax = plt.subplots(figsize=(6, 3))
sns.tsplot(data=sea_df, time="Time", unit="Unit",
           condition="Condition", value="Values", ax=ax)
sns.tsplot(data=sea_df, time="Time", unit="Unit",
           condition="Condition", value="Values", err_style="unit_traces")
sns.tsplot(data=sea_df, time="Time", unit="Unit",
           condition="Condition", value="Values", err_style="std_band")


# ------------------------------------------------------------------------------
# RUN STATS EVALUATION

EVAL_EP_DATA = ["mean_r_game_ep_RANDOM", "mean_r_game_ep_ASTAR"]

folders = glob.glob("experiments/exp_analise/" + "/*/")
groups = get_group(folders)

# Get sub checkpoint -
groups_eval = dict()
for group_key in groups.keys():
    group_base = os.path.basename(os.path.dirname(group_key +"/"))
    for folder_path in groups[group_key]:
        eval_folders = glob.glob(folder_path + "/checkpoint*/")
        for name in eval_folders:
            exp_name = os.path.basename(os.path.dirname(name))
            exp_name = "{}_{}".format(group_base, exp_name)
            if exp_name not in groups_eval:
                groups_eval[exp_name] = []
            groups_eval[exp_name].append(name)

all_folders = [item for sublist in groups_eval.values() for item in sublist]

# basenames = []
# for fld in all_folders:
#     dir2 = os.path.dirname(fld)
#     base_2 = os.path.basename(dir2)
#     dir1 = os.path.dirname(dir2)
#     base_1 = os.path.basename(dir1)
#     basenames.append("{}_{}".format(base_1, base_2))
basenames = all_folders

# -remove common substring
common_str = basenames[0]
for str_ in basenames[1:]:
    common_str = longestSubstringFinder(common_str, str_)

basenames = [re.sub(common_str, "", x) for x in basenames]
eval_ep_data = dict()
for filename in EVAL_EP_DATA:
    eval_ep_data[filename] = get_dataframe_gather(all_folders, filename, basename=basenames)
    eval_ep_data[filename] = eval_ep_data[filename].rename(columns={"Index":"Episode"})

groups_df = {t: [] for t in eval_ep_data.keys()}
group_name = {t: "" for t in eval_ep_data.keys()}

# CUSTUMIZE COLUMN NAME EX: FOR different_focus___batch_size_1024__p_focus_.0__auxiliary_tweaks_no_critic__1_
get_name = lambda base_name: "p_focus-{}_B-{}_{}_{}".format(
    re.findall(r'_p_focus_[.]?(\d+?(?=[.]?_))', base_name)[0],
    re.findall(r'batch_size_(\d+)', base_name)[0],
    re.findall("auxiliary_tweaks_(.*?(?=_\d))", base_name)[0],
    re.findall("(\d+.{1})_eval", base_name)[0])

get_name = lambda x: x
# Concatanate data from group
for k, v in groups_eval.items():
    name = get_name(v[0])

    values = {t: [] for t in eval_ep_data.keys()}
    for exp_ in v:
        name_ = get_name(v[0])
        if name != name_:
            print("different name {} -> {}".format(name, name_))
        col_name_ = re.sub(common_str, "", exp_)
        for k2, v2 in eval_ep_data.items():
            values[k2].append(v2[col_name_].values)

    for k2, v2 in values.items():
        groups_df[k2].append(pd.Series(np.concatenate(v2), name=name))
        group_name[k2] = name

for k,v in groups_df.items():
    groups_df[k] = pd.DataFrame(v).T

df = eval_ep_data
df = groups_df
col_ = col_without(df["mean_r_game_ep_RANDOM"],["Episode"])
rr = df["mean_r_game_ep_RANDOM"][col_]
ra = df["mean_r_game_ep_ASTAR"][col_]

col_ = col_contain(rr, "1024R")
col_.sort()
rr_ = change_df_col(rr[col_])
fig = rr_.plot(kind="box", rot=15, figsize=(6, 3))

col_ = col_contain(ra, "1024A")
col_.sort()
ra_ = change_df_col(ra[col_])
ra_.plot(kind="box", rot=15, figsize=(6, 3))
# ___________ PLOT GROUPING ------->
df = rr.copy()

# # SMOOTHING CONVOLUTION: &
# conv_by = 50
# for i in df.index:
#     df.loc[i] = df[i:i+conv_by].mean()
#
values = []
unit = []
time_stamp = []
condition = []
group_values = 1
df["timestamp"] = (df.index//group_values)*group_values
id_ = df.index - df["timestamp"]
id_ = np.apply_along_axis(lambda y: [str(i) for i in y], 0, id_.values)

for group_name in list(groups_eval.keys())[:]:
    for cl_ in groups_eval[group_name]:
        values += df[cl_].values.tolist()
        condition += [group_name] * len(df[cl_].values)
        unit += list(map(lambda x: cl_ + "_" + x, id_))
        time_stamp += df["timestamp"].tolist()


sea_df = pd.DataFrame({"Values":values,
                       "Unit":unit,
                       "Condition": condition,
                       "Time":time_stamp})

# TRANSFORM TO SEADATA STUFFF:::
paper_rc = {'lines.linewidth': 1, 'lines.markersize': 10}
sns.set_context("paper", rc = paper_rc)

# -Plot each individual trace
sns.tsplot(data=sea_df, time="Time", unit="Unit",
           condition="Condition", value="Values")


malmo_eval_STD_fine_m = ["/media/sparc-308/AwesomeData/res_fine_tune/STD_fine_tune_{}/checkpoint_max_eval_malmo/metrics.npy".format(i) for i in range(1,5)]
malmo_eval_STD_fine_at = ["/media/sparc-308/AwesomeData/res_fine_tune/STD_fine_tune_{}/checkpoint_max_eval_malmo/agent_type.npy".format(i) for i in range(1,5)]
malmo_eval_NObn_fine_m = ["/media/sparc-308/AwesomeData/res_fine_tune/bn_batch_comparisonB___batch_size_1024__auxiliary_tweaks_no_bn__{}_/checkpoint_max_eval_malmo/metrics.npy".format(i) for i in range(1,5)]
malmo_eval_NObn_fine_at = ["/media/sparc-308/AwesomeData/res_fine_tune/bn_batch_comparisonB___batch_size_1024__auxiliary_tweaks_no_bn__{}_/checkpoint_max_eval_malmo/agent_type.npy".format(i) for i in range(1,5)]

ep_r = malmo_eval_NObn_fine_m
ag_type = malmo_eval_NObn_fine_at
res_fine = {}
for ix in range(len(ep_r)):
    m = ep_r[ix]
    ag = ag_type[ix]
    r = np.array(np.load(m).item(0)["100k"]["buffer"])
    a = np.load(ag)
    res_fine[ix] = {
        "mean_RANDOM": np.ma.masked_array(r, mask=(1-a)).mean(),
        "var_RANDOM": np.ma.masked_array(r, mask=(1-a)).var(),
        "mean_ASTAR": np.ma.masked_array(r, mask=a).mean(),
        "var_ASTAR": np.ma.masked_array(r, mask=a).var(),
    }

mean_ = 0
var_ = 0
cnt = 0.

for key_ in res_fine[0].keys():
    sum_ = 0
    cnt = 0.

    for k, v in res_fine.items():
        sum_ += v[key_]
        cnt +=1

    print("{} _ {}".format(key_, sum_/cnt))
