# Village People, 2017

from env.env_docker_no_launch import EnvNoLaunch as Env

from env.VillagePeopleBuilders import PigChaseVillagePeopleBuilder18Binary
from synchronised_episodic_tcp_players import \
    ClientForSynchronisedEpisodicPlayers, NonProtocolException

from termcolor import colored as clr

# ---------------------------------------------------------
# from: https://stackoverflow.com/a/22348885

import signal


class timeout:
    def __init__(self, seconds=1, error_message='Timeout'):
        self.seconds = seconds
        self.error_message = error_message

    def handle_timeout(self, signum, frame):
        raise TimeoutError(self.error_message)

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)

    def __exit__(self, _type, value, traceback):
        signal.alarm(0)

# ---------------------------------------------------------


class MalmoDockerSocketClient(ClientForSynchronisedEpisodicPlayers):

    def __init__(self, clients, server, _id, verbose=1):
        self.max_ep_length = 25
        agent_role = 1
        _agent_builder = PigChaseVillagePeopleBuilder18Binary(agent_role)
        self.env = Env(clients, _id, _agent_builder)
        self.name = _id
        self.obs0 = None
        self.ep_length = None
        (host, port) = server
        super(MalmoDockerSocketClient, self).__init__(host, port, verbose)

    def reset_env(self):
        try:
            with timeout(seconds=120):
                self.obs0 = self.env.reset()
        except TimeoutError:
            raise NonProtocolException("reset exceeded 120s")
        self.ep_length = 0

    def get_initial_state(self):
        if self.obs0 is None:
            self.reset_env()
        return self.obs0

    def apply_action(self, action):
        try:
            with timeout(seconds=30):
                obs, reward, agent_done, _ = self.env.step(action)
        except TimeoutError:
            raise NonProtocolException("step exceeded 30s.")

        done = (agent_done[0] == 1) or self.ep_length >= self.max_ep_length
        self.ep_length += 1
        return obs, reward, done


def run_malmo_docker_socket_client(clients, server, _id, verbose=2):
    while True:
        try:
            print("[{:s}] STARTING:".format(str(_id)))
            client = MalmoDockerSocketClient(clients, server, _id,
                                             verbose=verbose)
            client.run()
        except NonProtocolException as e:
            try:
                client.env.close()
                del client
            except:
                pass
            print("[{:s}] DEAD:".format(str(_id)), clr(str(e), "yellow"))
            print("[{:s}] RESTARTING NO PROBLEM! Chill!".format(str(_id)))
        except Exception as e:
            print(clr("[{:s}] UNEXPECTED: {:s}".format(str(_id), str(e)), "red"))
