# Tudor Berariu @ Bitdefender, 2017

import socket
import torch
from termcolor import colored as clr
import time
import sys

class ServerForSynchronisedEpisodicPlayers(object):

    def __init__(self, clients_no, host, port, verbose=1):
        self.clients_no = clients_no
        self.verbose = verbose
        self.sock = sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((host, port))
        sock.listen(clients_no)

        self.ep_no = 0
        self.frame_no = 0
        self.connections = [None] * clients_no
        self.is_done = torch.ByteTensor(clients_no).fill_(0)

        if verbose > 0:
            print("[SERVER] " + "Listening...")

    def hello_message(self, i):
        return str.encode(
            "{:d},HELLO,{:d},{:d}".format(i, self.ep_no, self.frame_no)
        )

    def act_message(self, i, action=None):
        ep_no, frame_no = self.ep_no, self.frame_no
        if self.frame_no == 0:
            return str.encode(
                "{:d},INIT,{:d},{:d}".format(i, ep_no, frame_no)
            )
        else:
            return str.encode(
                "{:d},ACT,{:d},{:d},{:d}".format(i, ep_no, frame_no, action)
            )

    def reset_message(self, i):
        return str.encode(
            "{:d},RESET,{:d},{:d}".format(i, self.ep_no, self.frame_no)
        )

    @staticmethod
    def check_message(i):
        return str.encode("{:d},CHECK".format(i))

    def info(self, *args):
        if self.verbose > 0:
            print(clr("[SERVER] ", "blue"), *args)
            sys.stdout.flush()

    def debug(self, *args):
        if self.verbose > 1:
            print(clr("[SERVER] ", "red"), *args)
            sys.stdout.flush()


    def check_connection(self, conn: socket.socket, i: int) -> bool:
        """This function checks a particular connection"""
        if not isinstance(conn, socket.socket):
            return False
        try:
            conn.sendall(self.check_message(i))
            m = conn.recv(128)
            m = m.decode().split(",")
            ep = str(self.ep_no)
            frame = str(-1 if self.is_done[i] else self.frame_no)

            if m != [str(i), "CHECK", ep, frame]:
                if self.verbose > 0:
                    print(
                        "[SERVER] Check failed for " +\
                         "{:d} <= Got {:s} instead of {:s}".format(
                             i, ",".join(m),
                             ",".join([str(i), "CHECK", ep, frame])
                        )
                    )
                return False
            return True
        except Exception as e:
            if self.verbose > 0:
                print(
                    "[SERVER] Check failed for {:d} <= Exception: {:s}".format(
                        i, str(e)
                    )
                )
                print(e)
            return False

    def get_broken_connections(self) -> list:
        if self.verbose > 0:
            print("[SERVER] Checking all connections")
        need_restart = []

        for i in range(self.clients_no):
            conn = self.connections[i]
            if not self.check_connection(conn, i):
                if isinstance(conn, socket.socket):
                    try:
                        conn.close()
                    except:
                        pass
                need_restart.append(i)
        if self.verbose > 0:
            if need_restart:
                print("[SERVER] The following need restart: " +
                      clr(",".join(map(str, need_restart)), "red")
                )
            else:
                print("[SERVER] " + clr("All good!", "green"))
        return need_restart

    def accept_new_connection(self, i: int) -> socket.socket:
        verbose = self.verbose
        while True:
            try:
                conn, addr = self.sock.accept()
                if verbose > 0:
                    print("[SERVER] New connection from ", addr, " to ", i)
                hello_msg = self.hello_message(i)
                conn.sendall(hello_msg)
                conn.settimeout(30.)
                back_msg = conn.recv(1024)
                if back_msg == hello_msg:
                    self.info("Connection accepted from ", addr, " on ", i)
                    return conn
                else:
                    conn.close()
            except Exception as e:
                try:
                    conn.close()
                except:
                    pass
                self.info("Failed to establish connection because ",
                          clr(str(e), "red"))

    def _reset_all(self, connections_to_reset):
        all_good = True
        for i, conn in connections_to_reset:
            try:
                reset_msg = self.reset_message(i)
                conn.sendall(reset_msg)
            except Exception as e:
                if self.verbose > 0:
                    print("[SERVER] Failed to reset", i, "because", e)
                all_good = False

        for i, conn in connections_to_reset:
            try:
                reset_msg = self.reset_message(i)
                conn.settimeout(200.)
                back_msg = conn.recv(1024)
                if back_msg != reset_msg:
                    raise ValueError(str(back_msg) + " =\= " + str(reset_msg))
            except Exception as e:
                self.info("Client {:d} did not reset properly! {:s}".format(
                    i, clr(str(e), "red")
                ))
                try:
                    conn.close()
                except:
                    pass
                all_good = False
        return all_good

    def reset_all(self):
        self.info("Resetting all.")
        connections_to_reset = [(i, c) for (i, c) in enumerate(self.connections)]
        all_good = self._reset_all(connections_to_reset)

        if not all_good:
            bc = self.get_broken_connections()
            while bc:
                new_connections = []
                for i in bc:
                    conn = self.accept_new_connection(i)
                    self.connections[i] = conn
                    new_connections.append((i, conn))
                self._reset_all(new_connections)
                bc = self.get_broken_connections()

        self.reset(self.ep_no)

        if self.verbose > 0:
            print("[SERVER] All reset to episode ", self.ep_no)


    def close_all(self):
        for i, conn in self.connections:
            try:
                conn.close()
            except:
                pass

    def reset(self, ep_no):
        raise NotImplementedError

    def check_state(self, state):
        raise NotImplementedError

    def collect_states(self, actions=None):
        ep_no = self.ep_no
        frame_no = self.frame_no
        is_done = self.is_done
        actions = ([None] * self.clients_no) if actions is None else actions

        states, rewards = [], []
        all_good = True
        for i, conn in enumerate(self.connections):
            try:
                if not is_done[i]:
                    conn.sendall(self.act_message(i, actions[i]))
            except Exception as e:
                if self.verbose > 0:
                    print("[SERVER]", e)
                all_good = False

        for i, conn in enumerate(self.connections):
            try:
                if not is_done[i]:
                    msg = conn.recv(20480)
                    tries = 0
                    while tries < 5:
                        try:
                            data = torch.serialization.pickle.loads(msg)
                            break
                        except:
                            tries += 1
                            time.sleep(.1)
                            new_data = conn.recv(20480)
                            msg = msg + new_data

                    if data["ep"] != ep_no:
                        err = "Wrong episode {:d} =/= {:d} from {:d}.".format(
                            data["ep"], ep_no, i
                        )
                        raise Exception(err)
                    if data["frame"] != (frame_no + 1) and data["frame"] != -1:
                        err = "Wrong frame {:d} =/= {:d} from {:d}.".format(
                            data["frame"], frame_no + 1, i
                        )
                        raise Exception(err)
                    if not self.check_state(data["state"]):
                        raise Exception("Wrong state from {:d}.".format(i))
                    if data["frame"] == -1:
                        self.is_done[i] = 1

                    states.append(data["state"])
                    rewards.append(data["reward"])
                else:
                    states.append(self._dummy_state)
                    rewards.append(self._dummy_reward)
            except Exception as e:
                if self.verbose > 0:
                    print("[SERVER]", e)
                all_good = False

        if not all_good:
            return False
        return states, rewards

    def forget_current_episode(self):
        raise NotImplementedError

    def feedback(self, states, rewards):
        raise NotImplementedError

    def episode_finished(self, last_states, last_rewards):
        raise NotImplementedError

    def run(self, episodes_no):
        verbose = self.verbose
        if verbose > 0:
            print("[SERVER] Starting...")
        self.ep_no = 1
        self.frame_no = 0
        actions = None
        self.reset_all()
        last = time.time()
        while self.ep_no < episodes_no:
            result = self.collect_states(actions)
            if not result:
                if self.verbose:
                    err_msg = "Need to restart episode {:d}".format(self.ep_no)
                    print("[SERVER]", clr(err_msg, "red"))
                self.forget_current_episode()
                self.frame_no = 0
                self.is_done.fill_(0)
                self.reset_all()
            else:
                # -- Received states from all playing clients
                states, rewards = result
                actions = self.feedback(states, rewards)
                if self.is_done.all():
                    self.episode_finished(states, rewards)
                    now = time.time()
                    print("[SERVER] " +\
                          "Ep. {:d} of length {:d} finished in {:.2f}s.".format(
                        self.ep_no, self.frame_no, now - last
                    ))
                    last = now
                    self.ep_no += 1
                    self.frame_no = 0
                    self.is_done.fill_(0)
                    actions = None
                    self.reset_all()
                else:
                    self.frame_no += 1
        self.close_all()


class TestServer(ServerForSynchronisedEpisodicPlayers):

    def __init__(self, clients_no, host, port):
        self._dummy_state = torch.zeros(1, 18, 9, 9)
        self._dummy_reward = 0
        super(TestServer, self).__init__(clients_no, host, port)

    def feedback(self, states, rewards):
        import random
        return [random.randint(0, 3) for _ in range(len(states))]

    def check_state(self, state):
        return torch.is_tensor(state) and\
               state.size() == torch.Size([1, 18, 9, 9])

    def forget_current_episode(self):
        pass

    def episode_finished(self, last_states, last_rewards):
        pass

    def reset(self, ep_no):
        pass


def main():
    import sys
    assert len(sys.argv) >= 2

    host = ""
    port = 14022
    clients_no = int(sys.argv[1])

    if len(sys.argv) >= 4:
        host = sys.argv[2]
        port = int(sys.argv[3])
    elif len(sys.argv) == 3:
        try:
            port = int(sys.argv[2])
        except Exception as e:
            host = sys.argv[2]

    server = TestServer(clients_no, host, port)
    server.run(50000)

if __name__ == "__main__":
    main()
