from argparse import ArgumentParser
from threading import Thread, active_count
from time import sleep
from malmopy.agent import RandomAgent
from pig_chase.common import parse_clients_args, ENV_AGENT_NAMES,\
    ENV_TARGET_NAMES
from pig_chase.agent import PigChaseChallengeAgent, FocusedAgent
from pig_chase.environment import PigChaseEnvironment,\
    PigChaseSymbolicStateBuilder


def agent_factory(name, role, baseline_agent, clients):

    assert len(clients) >= 2, 'Not enough clients (need at least 2)'
    clients = parse_clients_args(clients)

    builder = PigChaseSymbolicStateBuilder()
    env = PigChaseEnvironment(clients, builder, role=role,
                              randomize_positions=True)

    if role == 0:
        agent = PigChaseChallengeAgent(name)

        if type(agent.current_agent) == RandomAgent:
            agent_type = PigChaseEnvironment.AGENT_TYPE_1
        else:
            agent_type = PigChaseEnvironment.AGENT_TYPE_2
        try:
            while True:
                _ = env.reset(agent_type)
                print(">> AA !")
        except Exception as e:
            print(str(e))
            print("A is out")
    else:

        if baseline_agent == 'astar':
            _ = FocusedAgent(name, ENV_TARGET_NAMES[0])
        else:
            _ = RandomAgent(name, env.available_actions)
        try:
            while True:
                _ = env.reset()
                print(">> BB !")
        except Exception as e:
            print(str(e))
            print("B is out")


def run_experiment(agents_def):
    assert len(agents_def) == 2, 'Not enough agents (required: 2, got: %d)'\
                % len(agents_def)

    processes = []
    for agent in agents_def:
        p = Thread(target=agent_factory, kwargs=agent)
        p.daemon = True
        p.start()

        # Give the server time to start
        if agent['role'] == 0:
            sleep(1)

        processes.append(p)

    try:
        # wait until only the challenge agent is left
        while active_count() > 2:
            sleep(0.1)
    except KeyboardInterrupt:
        print('Caught control-c - shutting down.')


if __name__ == '__main__':
    arg_parser = ArgumentParser('Pig Chase baseline experiment')
    arg_parser.add_argument('-t', '--type', type=str, default='astar',
                            choices=['astar', 'random'],
                            help='The type of baseline to run.')
    arg_parser.add_argument('-e', '--epochs', type=int, default=50,
                            help='Number of epochs to run.')
    arg_parser.add_argument('clients', nargs='*',
                            default=['172.17.0.2:10000', '172.17.0.3:10000'],
                            help='Minecraft clients endpoints (ip(:port)?)+')
    args = arg_parser.parse_args()

    agents = [{'name': agent, 'role': role, 'baseline_agent': args.type,
               'clients': args.clients}
              for role, agent in enumerate(ENV_AGENT_NAMES)]

    run_experiment(agents)

