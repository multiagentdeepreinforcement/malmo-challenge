# Tudor Berariu @ Bitdefender, 2017

import socket
import time
import torch
from termcolor import colored as clr
import sys

class NonProtocolException(Exception):
    pass


class ClientForSynchronisedEpisodicPlayers(object):

    def __init__(self, host, port, verbose=2):
        self.host = host
        self.port = port
        self.verbose = verbose
        self.my_id = "?"

    def get_initial_state(self):
        raise NotImplementedError

    def apply_action(self, action):
        raise NotImplementedError

    def reset_env(self):
        raise NotImplementedError

    def info(self, *args):
        if self.verbose > 0:
            name = ""
            if hasattr(self, "name"):
                name = str(self.name) + " "
            print(clr("[{:s}CLIENT {:s}] ".format(name, self.my_id), "blue"),
                  *args)
            sys.stdout.flush()

    def debug(self, *args):
        if self.verbose > 1:
            name = ""
            if hasattr(self, "name"):
                name = str(self.name) + " "
            print(clr("[{:s}CLIENT {:s}] ".format(name, self.my_id), "red"),
                  *args)
            sys.stdout.flush()

    def run(self):
        verbose = self.verbose
        server_info = (self.host, self.port)
        if verbose > 0:
            self.info("Running")

        while True:
            try:
                self.info("Retrying to conenct to ", server_info)
                conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                conn.connect(server_info)
                b_msg = conn.recv(1024)
                m = b_msg.decode().split(",")
                self.my_id = my_id = m[0]
                if m[1] != "HELLO":
                    raise ValueError("I was expecting a HELLO")
                ep_no, frame_no = int(m[2]), int(m[3])
                if frame_no != 0:
                    err = "[CLIENT {:s}] ".format(my_id) +\
                          "I was expecting frame 0, not {:d}".format(frame_no)
                    raise ValueError(err)
                conn.sendall(b_msg)

                self.info("Hanshake done with ", server_info)
                self.info("Waiting for commands")

                reset_was_done = False

                while True:
                    # conn.settimeout(300.)
                    m = conn.recv(1024).decode().split(",")
                    self.debug("Got", *m)
                    if m[0] != my_id:
                        err = "Wrong id {:s} instead of {:s}".format(
                            m[0], my_id
                        )
                        raise ValueError(err)

                    if m[1] == "CHECK":
                        back_info = [my_id, "CHECK", ep_no, frame_no]
                        answer = ",".join(map(str, back_info))
                        conn.sendall(str.encode(answer))

                    elif m[1] == "INIT":
                        if int(m[2]) != ep_no:
                            err = "Wrong episode {:s} in INIT".format(m[2])
                            raise ValueError(err)
                        if int(m[3]) != frame_no:
                            err = "Wrong frame {:s} in INIT".format(m[3])
                            raise ValueError(err)
                        frame_no += 1
                        conn.sendall(torch.serialization.pickle.dumps({
                            "state": self.get_initial_state(),
                            "frame": frame_no,
                            "ep": ep_no,
                            "reward": torch.LongTensor(1).fill_(0)
                        }))
                    elif m[1] == "ACT":
                        if int(m[2]) != ep_no:
                            err = "Wrong episode {:s} in ACT".format(m[2])
                            raise ValueError(err)
                        if int(m[3]) != frame_no:
                            err = "Wrong frame {:s} in ACT".format(m[3])
                            raise ValueError(err)
                        action = int(m[4])
                        state, reward, is_done = self.apply_action(action)
                        frame_no += 1
                        if is_done:
                            frame_no = -1

                        conn.sendall(torch.serialization.pickle.dumps({
                            "state": state,
                            "frame": frame_no,
                            "ep": ep_no,
                            "reward": reward
                        }))

                        if is_done:
                            time.sleep(.1)
                            self.reset_env()
                            reset_was_done = True


                    elif m[1] == "RESET":
                        ep_no, frame_no = int(m[2]), int(m[3])
                        if frame_no != 0:
                            err = "Expecting frame 0 in RESET, not {:d}".format(
                                frame_no
                            )
                            raise ValueError(err)
                        if not reset_was_done:
                            self.reset_env()
                        reset_was_done = False
                        back_info = [my_id, "RESET", ep_no, frame_no]
                        answer = ",".join(map(str, back_info))
                        conn.sendall(str.encode(answer))
                    else:
                        err = "Got unexpected message: " + ",".join(m)
                        raise Exception(err)

            except NonProtocolException as e:
                try:
                    conn.sendall("ERROR")  # -- Just in case server waits
                    conn.close()
                except:
                    pass
                raise e

            except Exception as e:
                try:
                    conn.sendall("ERROR")  # -- Just in case server waits
                    conn.close()
                except:
                    pass
                self.info("Exception: ", clr(str(e), "red"))
                time.sleep(2)
                self.my_id = "?"



class TestClient(ClientForSynchronisedEpisodicPlayers):

    def __init__(self, host, port):
        super(TestClient, self).__init__(host, port, verbose=2)

    def reset_env(self):
        time.sleep(2.1)

    def get_initial_state(self):
        return torch.rand(1, 18, 9, 9)

    def apply_action(self, action):
        import random
        # time.sleep(.01)
        return torch.rand(1, 18, 9, 9), random.randint(0, 5),\
               random.random() > .95


def main():
    import sys
    host = ""
    port = 14022

    if len(sys.argv) >= 3:
        host = sys.argv[1]
        port = int(sys.argv[2])
    elif len(sys.argv) == 2:
        try:
            port = int(sys.argv[1])
        except:
            host = sys.argv[1]

    client = TestClient(host, port)
    client.run()


if __name__ == "__main__":
    main()
