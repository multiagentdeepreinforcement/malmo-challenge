# Village People, 2017

# This script trains an agent on Malmo.

import torch
import time
from termcolor import colored as clr
from models import get_model
from utils import read_config
import numpy as np
import random
import torch.optim as optim

# from utils import AverageMeter
from worker_scripts.malmo_docker_socket_server import run_malmo_socket_server



def print_info(message):
    print(clr("[MAIN] ", "yellow") + message)



def main():
    print_info("Booting...")
    cfg = read_config()

    # -- Print important conf info
    print_info("\nCONFIG >")
    print_info(clr("P_FOCUS_minecraft: {}".format(cfg.envs.minecraft.p_focus),
                   "red"))

    # -- Configure Torch
    if cfg.general.seed > 0:
        torch.manual_seed(cfg.general.seed)
        np.random.seed(cfg.general.seed)
        random.seed(cfg.general.seed)
        torch.cuda.manual_seed_all(cfg.general.seed)

    print_info("Torch setup finished.")

    # -- Configure model
    model = get_model(cfg.model.name)(cfg.model)
    if cfg.general.use_cuda:
        model.cuda()

    Optimizer = getattr(optim, cfg.training.algorithm)
    optim_args = vars(cfg.training.algorithm_args)
    optimizer = Optimizer(model.parameters(), **optim_args)

    if isinstance(cfg.model.load, str):
        checkpoint = torch.load(cfg.model.load)
        if "max_r_ep" in checkpoint:
            reward = checkpoint['max_r_ep']
        else:
            reward = checkpoint['reward']
        print("Loading Model: {} Mean reward/ episode: {}"
              "".format(cfg.model.load, reward))
        model.load_state_dict(checkpoint['state_dict'])
        # if "optim_dict" in checkpoint:
        #    optimizer.load_state_dict(checkpoint['optim_dict'])

    print_info("Model {:s} initalized.".format(clr(cfg.model.name, "red")))

    # -- Shared objects
    objects = {
        "model": model,
        "optimizer": optimizer
    }

    # -- Start training

    print_info("Training starts now!")
    start_time = time.time()
    run_malmo_socket_server(objects, cfg)
    total_time = time.time() - start_time
    print_info("Training and no evluation done in {:.2f}!".format(total_time))


if __name__ == "__main__":
    main()
