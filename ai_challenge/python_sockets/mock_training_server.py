import socket
import sys
import random
from copy import copy


def main():
    HOST = "127.0.0.1"
    PORT = 15022
    EPS_NO = 3000

    connections = []

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    s.bind((HOST, PORT))
    s.listen(1)

    for i in range(int(sys.argv[1])):
        conn, addr = s.accept()
        print("Someone connected from {}".format(addr))
        connections.append(conn)

    # --- ACTUAL PLAYING

    for ep in range(EPS_NO):
        for conn in connections:
            conn.send(str.encode("RESET"))

        print("Sent RESET to slaves")
        live_connections = copy(connections)

        while live_connections:
            for conn in live_connections:
                msg = conn.recv(8192)
                info = msg[0:1].decode()
                print("Received info", info)

                if info == "P":
                    conn.send(str.encode(str(random.randint(0, 2))))
                    print("Sent action")
                elif info == "D":
                    print("Episode done")
                    live_connections.remove(conn)
                    break
                else:
                    assert False, "Sugi pula"
    print("FINISHED")
    conn.sendall(str.encode("FINISHED"))


    # --------


    conn.close()
    s.close()



if __name__ == "__main__":
    main()
