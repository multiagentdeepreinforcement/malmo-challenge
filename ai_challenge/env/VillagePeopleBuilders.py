# Village People, 2017

from __future__ import absolute_import

import numpy as np

from pig_chase.common import ENV_BOARD, Entity, ENV_ENTITIES, \
    ENV_BOARD_SHAPE, ENV_AGENT_NAMES, ENV_TARGET_NAMES

from malmopy.environment.malmo import MalmoStateBuilder

from .artificial_malmo import BLOCK_TYPE
from .artificial_malmo import yaw_to_angle

from malmopy.agent import BaseAgent
from pig_chase.environment import PigChaseEnvironment

import torch
from tkinter import ttk, Canvas, W
from tkinter import Tk
import time

CELL_WIDTH = 33

from malmopy.agent.gui import GuiAgent


def transform_angle(agent):
    agent['yaw'] = (((((int(agent['yaw']) - 45) % 360) // 90) - 1) % 4) + 2
    return agent['yaw']


def getAngle(agent):
    return (((((int(agent['yaw']) - 45) % 360) // 90) - 1) % 4) + 2


class PigChaseVillagePopleAgent(BaseAgent):
    """
    Village Pople Official Agent
    -- is_training -> False !
    -- Assumes observations got from the PigChaseVillagePeopleBuilder18Binary
    """

    def __init__(self, name, nb_actions, agent, use_cuda=False,
                 visualizer=None):
        super(PigChaseVillagePopleAgent, self).__init__(name, nb_actions,
                                                        visualizer)

        self.dtype = torch.LongTensor([])
        if use_cuda:
            self.dtype = self.dtype.cuda()

        self.agent = agent

        self._visualize = False

    def act(self, new_state, reward, done, is_training=False):
        dtype = self.dtype

        state_ = torch.LongTensor(new_state).type_as(dtype).unsqueeze(0)
        reward_ = torch.FloatTensor([reward]).type_as(dtype)
        done_ = torch.LongTensor([int(done)]).type_as(dtype)

        if not done:
            act = self.agent.act(state_, reward_, done_, False)
        else:
            state_empty = state_.clone()
            state_empty.fill_(0)
            _ = self.agent.act(state_empty, reward_, done_, False)
            reward_[0] = 0
            done_[0] = 0
            act = self.agent.act(state_, reward_, done_, False)

        return act[0]

    def save(self, out_dir):
        pass

    def load(self, out_dir):
        pass

    def inject_summaries(self, idx):
        pass

    def reset(self):
        pass

class PigChaseVillagePopleAgentGUI(BaseAgent):

    """
    Village Pople Official Agent
    -- is_training -> False !
    -- Assumes observations got from the PigChaseVillagePeopleBuilder18Binary
    """
    def __init__(self, name, nb_actions, agent, use_cuda=False,
                 visualizer=None):
        super(PigChaseVillagePopleAgentGUI, self).__init__(name, nb_actions,
                                                        visualizer)

        self.dtype = torch.LongTensor([])
        if use_cuda:
            self.dtype = self.dtype.cuda()

        self.agent = agent

        self._visualize = True

    def build_viz(self, environment):
        self._max_episodes = 0
        self._max_actions = 25
        self._action_taken = 0
        self._episode = 1
        self._scores = []
        self._rewards = []
        self._episode_has_ended = False
        self._episode_has_started = False
        self._quit_event = quit


        if not environment.recording:
            environment.recording = True

        size = (640, 480)

        self._env = environment
        self._tick = 10

        self._root = Tk()
        self._root.wm_title = "Game"
        self._root.resizable(width=False, height=False)
        self._root.geometry = "%dx%d" % size

        self._build_layout(self._root)

    def show(self):
        self._root.mainloop()

    def act(self, new_state, reward, done, is_training=False):
        dtype = self.dtype

        state_ = torch.LongTensor(new_state).type_as(dtype).unsqueeze(0)
        reward_ = torch.FloatTensor([reward]).type_as(dtype)
        done_ = torch.LongTensor([int(done)]).type_as(dtype)

        if not done:
            act = self.agent.act(state_, reward_, done_, False)
        else:
            state_empty = state_.clone()
            state_empty.fill_(0)
            _ = self.agent.act(state_empty, reward_, done_, False)
            reward_[0] = 0
            done_[0] = 0
            act = self.agent.act(state_, reward_, done_, False)

        return act[0]

    def save(self, out_dir):
        pass

    def load(self, out_dir):
        pass

    def inject_summaries(self, idx):
        pass

    def reset(self):
        pass

    def _build_layout(self, root):
        # Left part of the GUI, first person view
        self._first_person_header = ttk.Label(root, text='First Person View', font=(None, 14, 'bold')) \
            .grid(row=0, column=0)
        self._first_person_view = ttk.Label(root)
        self._first_person_view.grid(row=1, column=0, rowspan=10)

        # Right part, top
        self._first_person_header = ttk.Label(root, text='Symbolic View', font=(None, 14, 'bold')) \
            .grid(row=0, column=1)
        self._symbolic_view = Canvas(root)
        self._symbolic_view.configure(width=ENV_BOARD_SHAPE[0]*CELL_WIDTH,
                                      height=ENV_BOARD_SHAPE[1]*CELL_WIDTH)
        self._symbolic_view.grid(row=1, column=1)

        # Bottom information
        self._information_panel = ttk.Label(root, text='Game stats_leRMS', font=(None, 14, 'bold'))
        self._current_episode_lbl = ttk.Label(root, text='Episode: 0', font=(None, 12))
        self._cum_reward_lbl = ttk.Label(root, text='Score: 0', font=(None, 12, 'bold'))
        self._last_action_lbl = ttk.Label(root, text='Previous action: None', font=(None, 12))
        self._action_done_lbl = ttk.Label(root, text='Actions taken: 0', font=(None, 12))
        self._action_remaining_lbl = ttk.Label(root, text='Actions remaining: 0', font=(None, 12))

        self._information_panel.grid(row=2, column=1)
        self._current_episode_lbl.grid(row=3, column=1, sticky=W, padx=20)
        self._cum_reward_lbl.grid(row=4, column=1, sticky=W, padx=20)
        self._last_action_lbl.grid(row=5, column=1, sticky=W, padx=20)
        self._action_done_lbl.grid(row=6, column=1, sticky=W, padx=20)
        self._action_remaining_lbl.grid(row=7, column=1, sticky=W, padx=20)
        self._overlay = None

        # Main rendering callback
        self._user_pressed_enter = False

        # UI Update callback
        root.after(self._tick, self._poll_frame)
        root.after(1000, self._on_episode_start)

        root.focus()

    def _draw_arrow(self, yaw, x, y, cell_width, colour):
        if yaw == 0.:
            x1, y1 = (x + .15) * cell_width, (y + .15) * cell_width
            x2, y2 = (x + .5) * cell_width, (y + .4) * cell_width
            x3, y3 = (x + .85) * cell_width, (y + .85) * cell_width

            self._symbolic_view.create_polygon(x1, y1, x2, y3, x3, y1, x2, y2, fill=colour)
        elif yaw == 90.:
            x1, y1 = (x + .15) * cell_width, (y + .15) * cell_width
            x2, y2 = (x + .6) * cell_width, (y + .5) * cell_width
            x3, y3 = (x + .85) * cell_width, (y + .85) * cell_width

            self._symbolic_view.create_polygon(x1, y2, x3, y1, x2, y2, x3, y3, fill=colour)
        elif yaw == 180.:
            x1, y1 = (x + .15) * cell_width, (y + .15) * cell_width
            x2, y2 = (x + .5) * cell_width, (y + .6) * cell_width
            x3, y3 = (x + .85) * cell_width, (y + .85) * cell_width

            self._symbolic_view.create_polygon(x1, y3, x2, y1, x3, y3, x2, y2, fill=colour)
        else:
            x1, y1 = (x + .15) * cell_width, (y + .15) * cell_width
            x2, y2 = (x + .4) * cell_width, (y + .5) * cell_width
            x3, y3 = (x + .85) * cell_width, (y + .85) * cell_width

            self._symbolic_view.create_polygon(x1, y3, x2, y2, x1, y1, x3, y2, fill=colour)

    def _poll_frame(self):
        """
        Main callback for UI rendering.
        Called at regular intervals.
        The method will ask the environment to provide a frame if available (not None).
        :return:
        """
        cell_width = CELL_WIDTH
        circle_radius = 10

        # are we done?
        if self._env.done and not self._episode_has_ended:
            self._on_episode_end()

        # build symbolic view
        board = None
        if self._env is not None:
            board, _ = self._env._internal_symbolic_builder.build(self._env)
        if board is not None:
            board = board.T
            self._symbolic_view.delete('all')  # Remove all previous items from Tkinter tracking
            width, height = board.shape
            for x in range(width):
                for y in range(height):
                    cell_contents = str.split(str(board[x][y]), '/')
                    for block in cell_contents:
                        if block == 'sand':
                            self._symbolic_view.create_rectangle(x * cell_width, y * cell_width,
                                                                 (x + 1) * cell_width, (y + 1) * cell_width,
                                                                 outline="black", fill="orange", tags="square")
                        elif block == 'grass':
                            self._symbolic_view.create_rectangle(x * cell_width, y * cell_width,
                                                                 (x + 1) * cell_width, (y + 1) * cell_width,
                                                                 outline="black", fill="lawn green", tags="square")
                        elif block == 'lapis_block':
                            self._symbolic_view.create_rectangle(x * cell_width, y * cell_width,
                                                                 (x + 1) * cell_width, (y + 1) * cell_width,
                                                                 outline="black", fill="black", tags="square")
                        elif block == ENV_TARGET_NAMES[0]:
                            self._symbolic_view.create_oval((x + .5) * cell_width - circle_radius,
                                                            (y + .5) * cell_width - circle_radius,
                                                            (x + .5) * cell_width + circle_radius,
                                                            (y + .5) * cell_width + circle_radius,
                                                            fill='pink')
                        elif block == self.name:
                            yaw = self._env._world_obs['Yaw'] % 360
                            self._draw_arrow(yaw, x, y, cell_width, 'red')
                        elif block == ENV_AGENT_NAMES[0]:
                            # Get yaw of other agent:
                            entities = self._env._world_obs[ENV_ENTITIES]
                            other_agent = list(
                                map(Entity.create, filter(lambda e: e['name'] == ENV_AGENT_NAMES[0], entities)))
                            if len(other_agent) == 1:
                                other_agent = other_agent.pop()
                                yaw = other_agent.yaw % 360
                                self._draw_arrow(yaw, x, y, cell_width, 'blue')

        # display the most recent frame
        frame = self._env.frame
        if frame is not None:
            from PIL import ImageTk
            self._first_person_view.image = ImageTk.PhotoImage(image=frame)
            self._first_person_view.configure(image=self._first_person_view.image)
            self._first_person_view.update()

        self._first_person_view.update()

        # # process game state (e.g., has the episode started?)
        # if self._episode_has_started and time.time() - self._episode_start_time < 3:
        #     if not hasattr(self, "_init_overlay") or not self._init_overlay:
        #         self._create_overlay()
        #     self._init_overlay.delete("all")
        #     self._init_overlay.create_rectangle(
        #         10, 10, 590, 290, fill="white", outline="red", width="5")
        #     self._init_overlay.create_text(
        #         300, 80, text="Get ready to catch the pig!",
        #         font=('Helvetica', '18'))
        #     self._init_overlay.create_text(
        #         300, 140, text=str(3 - int(time.time() - self._episode_start_time)),
        #         font=('Helvetica', '18'), fill="red")
        #     self._init_overlay.create_text(
        #         300, 220, width=460,
        #         text="How to play: \nUse the left/right arrow keys to turn, "
        #              "forward/back to move. The pig is caught if it is "
        #              "cornered without a free block to escape to.",
        #         font=('Helvetica', '14'), fill="black")
        #     self._root.update()
        #
        # elif self._episode_has_ended:
        #
        #     if not hasattr(self, "_init_overlay") or not self._init_overlay:
        #         self._create_overlay()
        #     self._init_overlay.delete("all")
        #     self._init_overlay.create_rectangle(
        #         10, 10, 590, 290, fill="white", outline="red", width="5")
        #     self._init_overlay.create_text(
        #         300, 80, text='Finished episode %d of %d' % (self._episode, self._max_episodes),
        #         font=('Helvetica', '18'))
        #     self._init_overlay.create_text(
        #         300, 120, text='Score: %d' % sum(self._rewards),
        #         font=('Helvetica', '18'))
        #     if self._episode > 1:
        #         self._init_overlay.create_text(
        #             300, 160, text='Average over %d episodes: %.2f' % (self._episode, np.mean(self._scores)),
        #             font=('Helvetica', '18'))
        #     self._init_overlay.create_text(
        #         300, 220, width=360,
        #         text="Press RETURN to start the next episode, ESC to exit.",
        #         font=('Helvetica', '14'), fill="black")
        #     self._root.update()
        #
        # elif hasattr(self, "_init_overlay") and self._init_overlay:
        #     self._destroy_overlay()

        # trigger the next update
        self._root.after(self._tick, self._poll_frame)

    def _create_overlay(self):
        self._init_overlay = Canvas(self._root, borderwidth=0, highlightthickness=0, width=600, height=300, bg="gray")
        self._init_overlay.place(relx=0.5, rely=0.5, anchor='center')

    def _destroy_overlay(self):
        self._init_overlay.destroy()
        self._init_overlay = None

    def _on_episode_start(self):
        self._episode_has_ended = False
        self._episode_has_started = True
        self._episode_start_time = time.time()
        self._on_experiment_updated(None, 0, self._env.done)

    def _on_episode_end(self):
        # do a turn to ensure we get the final reward and observation
        no_op_action = 0
        _, reward, done = self._env.do(no_op_action)
        self._action_taken += 1
        self._rewards.append(reward)
        self._on_experiment_updated(no_op_action, reward, done)

        # report scores
        self._scores.append(sum(self._rewards))
        self.visualize(self._episode, 'Reward', sum(self._rewards))

        # set flags to start a new episode
        self._episode_has_started = False
        self._episode_has_ended = True

    def _on_experiment_updated(self, action, reward, is_done):
        self._current_episode_lbl.config(text='Episode: %d' % self._episode)
        self._cum_reward_lbl.config(text='Score: %d' % reward)
        self._last_action_lbl.config(text='Previous action: %s' % action)
        self._action_done_lbl.config(text='Actions taken: {0}'.format(self._action_taken))
        self._action_remaining_lbl.config(text='Actions remaining: %d' % (self._max_actions - self._action_taken))
        self._first_person_view.update()

    def _quit(self):
        import sys
        self._quit_event.set()
        self._root.quit()
        sys.exit()


class PigChaseVillagePeopleBuilder18Binary(MalmoStateBuilder):
    """
    This class build a symbolic representation of the current environment.
    GENERATES A MAP OF 9x9x18 binary maps
    x3 - block type ["grass", "sand", "lapis_block"]
    x5 - Player agent - 1x location + x4 (Direction type) [N, E, S, V]
    x5 - Opponent agent -  - ... -
    x5 - Pig -  - ... -
    """

    def __init__(self, player_idx):
        """
        :param player_idx: must specify the player for whom the map is built
        Order is important
        """
        self.player = ENV_AGENT_NAMES[player_idx]
        self.opponent = ENV_AGENT_NAMES[(player_idx + 1) % len(ENV_AGENT_NAMES)]
        self.pig = ENV_TARGET_NAMES[0]

        self.block_board_size = (len(BLOCK_TYPE),) + ENV_BOARD_SHAPE
        self.entity_board_size = (5,) + ENV_BOARD_SHAPE
        self.block_type = BLOCK_TYPE

        self.last_binary_view_0 = self.last_binary_view_1 = \
            self.last_binary_view_2 = np.zeros(self.entity_board_size,
                                               dtype=int)

    def binary_view_entity(self, entity):
        """
        Receives a dictionary with entity data:
        row, col, direction
        """
        i, j, direction = entity["row"], entity["col"], entity["direction"]

        board_binary = np.zeros(self.entity_board_size, dtype=int)
        board_binary[0][i][j] = 1
        board_binary[direction + 1][i][j] = 1
        return board_binary

    def build(self, environment):
        """
        Return a symbolic view of the board

        :param environment Reference to the pig chase environment
        :return (board, entities) where board is an array of shape (9, 9)
        """
        assert isinstance(environment,
                          PigChaseEnvironment), \
            'environment is not a Pig Chase Environment instance'

        block_board_size = self.block_board_size
        block_type = self.block_type

        world_obs = environment.world_observations

        if world_obs is None or ENV_BOARD not in world_obs:
            return None

        # Generate symbolic view
        board = np.array(world_obs[ENV_BOARD], dtype=object).reshape(
            ENV_BOARD_SHAPE)

        board_binary = np.zeros(block_board_size, dtype=int)
        for ix, block_type in enumerate(block_type):
            board_binary[ix] = board == block_type

        entities = world_obs[ENV_ENTITIES]
        entities_dict = dict()
        for entity in entities:
            entities_dict[entity["name"]] = dict(
                {"row": int(entity['z'] + 1),
                 "col": int(entity['x']),
                 "direction": yaw_to_angle(entity['yaw'])})

        if self.player in entities_dict:
            binary_view_0 = self.binary_view_entity(entities_dict[self.player])
        else:
            binary_view_0 = np.zeros(self.entity_board_size, dtype=int)

        if self.opponent in entities_dict:
            binary_view_1 = self.binary_view_entity(
                entities_dict[self.opponent])
        else:
            binary_view_1 = np.zeros(self.entity_board_size, dtype=int)

        if self.pig in entities_dict:
            binary_view_2 = self.binary_view_entity(entities_dict[self.pig])
        else:
            binary_view_2 = np.zeros(self.entity_board_size, dtype=int)

        self.last_binary_view_0 = binary_view_0
        self.last_binary_view_1 = binary_view_1
        self.last_binary_view_2 = binary_view_2

        game_view = np.concatenate([board_binary, binary_view_0,
                                    binary_view_1, binary_view_2])

        return game_view
