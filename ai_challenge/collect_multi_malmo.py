import sys
import time

import torch.multiprocessing as mp
from termcolor import colored as clr
from worker_scripts.collect_from_malmo_docker import collect_from_malmo_docker

if __name__ == "__main__":
    procs = []
    sleep_between_processes = False

    for i in range(int(sys.argv[1])):
        simulator_proc = mp.Process(target=collect_from_malmo_docker,
                                    args=("gtx-titan3", 15022, i))
        procs.append(simulator_proc)

    for p in procs:
        p.start()
        if sleep_between_processes:
            time.sleep(sleep_between_processes)

    for p in procs:
        p.join()
