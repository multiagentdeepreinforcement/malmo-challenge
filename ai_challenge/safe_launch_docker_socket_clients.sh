#!/bin/bash

die() {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 3 ] || die "3 arguments required (dckrs no, host, port), $# provided"

echo $1 | grep -E -q '^[0-9]+$' || die "Games no. must be int, not shit:, $1"
echo $3 | grep -E -q '^[0-9]+$' || die "Port must be int, not shit: $3"

docker stop $(docker ps -qa)

for i in `seq 1 $1`; do
 z=`expr $i + 9999`
 echo "Launching on port: $z"
 docker run -d -p $z:10000 malmo:latest &
done

alive=`docker ps -q | wc -l`
while [ $alive -lt "1" ] ; do
 sleep 1
 alive=`docker ps -q | wc -l`
done


m=`docker inspect -f '{{ .NetworkSettings.IPAddress }}' $(docker ps -q) | wc -l`

while [ $m -lt $1 ]
do
echo "Having just $m out of $1"
sleep 1
m=`docker inspect -f '{{ .NetworkSettings.IPAddress }}' $(docker ps -q) | wc -l`
done

rm -f _ips1
touch _ips1

docker inspect -f '{{ .NetworkSettings.IPAddress }}' $(docker ps -q) > _ips1
ds1=`cat _ips1`

for ip in `cat _ips1`; do
 result="no"
 while [ "$result" != "yes" ]
 do
  result=`python is_there_malmo_listening.py $ip 10000`
  echo "Is there Malmo on $ip:10000? $result"
  sleep 5
 done
done


for i in `seq 1 $1`; do
 z=`expr $i + 19999`
 echo "Launching on port: $z"
 docker run -d -p $z:10000 malmo:latest
done

m=`docker inspect -f '{{ .NetworkSettings.IPAddress }}' $(docker ps -q) | wc -l`
n=`expr 2 \* $1`

while [ $m -lt $n ]; do
 echo "Having just $m out of $n"
 sleep 1
 m=`docker inspect -f '{{ .NetworkSettings.IPAddress }}' $(docker ps -q) | wc -l`
done

docker inspect -f '{{ .NetworkSettings.IPAddress }}' $(docker ps -q) > _ips

rm -f _ips2
touch _ips2

for ip in `cat _ips`; do
 old=0
 for ip1 in `cat _ips1`; do
  if [ "$ip" == "$ip1" ] ; then
   old=1
  fi
 done
 if [ "$old" -lt "1" ] ; then
  echo "$ip" >> _ips2
 fi
done

ds2=`cat _ips2`

for ip in `cat _ips2`; do
 result="no"
 while [ "$result" != "yes" ]
 do
  result=`python is_there_malmo_listening.py $ip 10000`
  echo "Is there Malmo on $ip:10000? $result"
  sleep 5
 done
done


ds1=(${ds1})
size1=${#ds1[*]}
ds2=(${ds2})
size2=${#ds2[*]}

echo "$size1 started and then another $size2 started..."


echo "Starting players too..."

timestamp=`date +%s`
mkdir -p ./nohup_outputs
mkdir -p ./docker_control

echo "The id of this experiment is $timestamp. Godspeed!"

kill_script="./docker_control/"$timestamp"_kill_all.sh"
process_list="./docker_control/"$timestamp"_processes"
touch $kill_script
touch $process_list

i=0
while [ "$i" -lt "$size1" ]
do
    echo "Pairing ${ds1[$i]} and ${ds2[$(($i))]}"
    o="./nohup_outputs/"$timestamp"_out_"$i
    nohup python launch_docker_socket_client.py "$2" $3 $i  ${ds1[$i]} ${ds2[$(($i))]} >$o 2>&1 &
    echo "$!" >> $process_list
    let "i=$i+1"
done

echo "process_list="$process_list >> $kill_script
cat killer.sh >> $kill_script
